<?php

namespace VoDBService;
use Scrapper;
use Library;
use Exception;

class StorageService
{
	private $scrapper;
	private $library;

    public function __construct() {
		$this->scrapper = Scrapper\CachedScrapperFactory::getInstance();
		$this->library = Library\LibraryFactory::getInstance();
    }

	/**
	 * @param $id
	 * @return null
	 * @throws Exception
	 */
	public function storeMovieById($id) {
        $movieDetail = $this->scrapper->getMovieById($id);

		if (empty($movieDetail)) {
			Throw new Exception("No data for movie id " . $id);
			return null;
		}

		$fileList = array();

		// Download backdrop
		if (isset($movieDetail['backdrop_path'])&& $movieDetail['backdrop_path'] !== "") {
			$backdropUrl = "http://image.tmdb.org/t/p/w300" . $movieDetail['backdrop_path'];
			$backdrop = $this->scrapper->downloadPoster($backdropUrl);
			$fileName = "pics/backdrops" . $movieDetail['backdrop_path'];
			if (file_exists($fileName)) {
				array_push($fileList, $fileName);
			} else {
				$fp = fopen($fileName, 'w');
				if ($fp !== null) {
					if (fwrite($fp, $backdrop)) {
						fclose($fp);
						array_push($fileList, $fileName);
					}
				}
			}
		}

		// Download poster
		if (isset($movieDetail['poster_path']) && $movieDetail['poster_path'] !== "") {
			$posterUrl = "http://image.tmdb.org/t/p/w780" . $movieDetail['poster_path'];
			$poster = $this->scrapper->downloadPoster($posterUrl);
			$fileName = "pics/posters/" . $movieDetail['poster_path'];
			if (file_exists($fileName)) {
				array_push($fileList, $fileName);
			} else {
				$fp = fopen($fileName, 'w');
				if ($fp !== null) {
					if(fwrite($fp, $poster)) {
						fclose($fp);
						array_push($fileList, $fileName);
					}
				}
			}
		}

		// Store movie to DB only if the two previously stored pictures are actually on disk
		if (count($fileList) == 2) {
			$this->library->saveToLibrary($movieDetail, $movieDetail['poster_path']);
			echo("Movie " . $movieDetail['title'] . " id(" . $movieDetail['id'] . ") stored\n");
		} else {
			foreach($fileList as $file) {
				unlink($file);
			}
			Throw new Exception("Missing poster or backdrop for movie " . $movieDetail['title'] . " id(" . $movieDetail['id'] . "), storage aborted\n");
		}
	}

	/**
	 * @param array $movieList
	 */
	public function fillDatabase(array $movieList) {
		$count = 0;
		foreach($movieList as $movie) {
			try {
				$searchResults = $this->scrapper->searchMovieByName($movie);
				foreach($searchResults as $result) {
					$this->storeMovieById($result['id']);
					$count++;
				}
			} catch (Exception $e) {
				echo($e->getMessage());
			}
		}
		echo($count . " movies added to database\n");
	}

	/**
	 * @param int $limit
	 */
	public function storePopularity($limit = 0) {
	}
}