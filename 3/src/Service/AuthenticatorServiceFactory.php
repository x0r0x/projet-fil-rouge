<?php

namespace VoDBService;

class AuthenticatorServiceFactory
{
    /**
     * @return AuthenticatorService
     */
    public static function getInstance() {
        return new AuthenticatorService();
    }
}