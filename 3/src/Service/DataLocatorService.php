<?php

namespace VoDBService;
use Library\LibraryFactory;
use Scrapper\CachedScrapperFactory;
use Exception;

class DataLocatorService
{
    public static $sourceList = array("internal" => 0, "external" => 1);
    private $source;
    private $library;

    public function __construct($src) {
        if (!array_key_exists($src, self::$sourceList)) {
            Throw new Exception("Source not recognized");
        }

        $this->source = self::$sourceList[$src];
        $this->library = LibraryFactory::getInstance();
    }

    /**
     * @param $movieCollection
     * @return array
     */
    public function adaptMovieFormat($movieCollection) {
        $convertedMovieCollection = array();
        foreach ($movieCollection as $movie) {
            // Adaptation des clefs pour matcher avec celles de themoviedb, temporaire
            $movie['backdrop_path'] = $movie['Backdrop'];
            $movie['poster_path'] = '/' . $movie['Poster'];
            $movie['overview'] = $movie['Synopsis'];
            $movie['release_date'] = $movie['Year'];
            $movie['popularity'] = $movie['Rating'];
            unset($movie['Backdrop']);
            unset($movie['Poster']);
            unset($movie['Synopsis']);
            unset($movie['Year']);
            unset($movie['Rating']);
            $director = $this->library->findDirectorById($movie['DirectorId']);
            $movie['director'] = $director->getFirstname(). ' ' . $director->getLastname();

            // Ajout des catégories à la clef genres
            $categories = array();
            foreach ($this->library->findCategoryByMovieTitle($movie['Title']) as $category) {
                $category['name'] = $category['CategoryName'];
                unset($category['CategoryName']);
                array_push($categories, $category);
            }
            $movie['genres'] = $categories;

            array_push($convertedMovieCollection, array_change_key_case($movie, CASE_LOWER));
        }
        return $convertedMovieCollection;
    }

    /**
     * @param $limit
     */
    public function getPopular($limit) {
        if ($this->source === self::$sourceList["internal"]) {
            $popularMovieCollection = $this->library->findPopularMovies(100);
            echo(json_encode($this->adaptMovieFormat($popularMovieCollection)));
        } else if ($this->source === self::$sourceList["external"]) {
            $scrapper = CachedScrapperFactory::getInstance();
            echo(json_encode($scrapper->getPopularMovies($limit)));
        }
    }

    /**
     * @param $pattern
     * @return array|mixed
     */
    public function search($pattern) {
        if ($this->source === self::$sourceList["internal"]) {
            $movieSearchResultCollection = $this->library->findMovieByTitle($pattern);
            return $this->adaptMovieFormat($movieSearchResultCollection);
          } else if ($this->source === self::$sourceList["external"]) {
            $scrapper = CachedScrapperFactory::getInstance();
            return $scrapper->searchMovieByName($pattern);
        }
    }
}