<?php

namespace Scrapper;

class CachedScrapperFactory
{
    /**
     * @return CachedScrapper
     */
    static function getInstance()
    {
        $cachedScrapper = new CachedScrapper(new Scrapper());
        return $cachedScrapper;
    }
}
   