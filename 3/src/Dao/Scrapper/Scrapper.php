<?php

namespace Scrapper;
use Exception;
use GuzzleHttp\Client;

class Scrapper 
{
    const HTTP_REQUEST_OK        = 200;
    const HTTP_MOVIE_NOT_FOUND   = 404;
    const TOO_MANY_REQUEST       = 429;
    const POSTER_PATH            = "http://image.tmdb.org/t/p/";

    private $ApiUrlBase     	= "http://api.themoviedb.org";
    private $ApiKeyParam    	= "api_key=92b2ed9804f782fb8bd8689b3b85faea";
	private $httpRequest;


	function __construct() {
		$this->httpRequest = new Client(['verify' => false]);
	}
	
    /** Retourne un tableau contenant les données JSON pour GET $url (ou les données brutes si $jsonEncode==false)
     * @param $url
     * @param bool $jsonEncode
     * @return mixed|\Psr\Http\Message\StreamInterface
     * @throws Exception
     */
	private function requestApi($url, $jsonEncode=true) {
	    if (empty($url)) {
            throw new Exception("\$url ne peut être vide");
        }

	    $res = null;

        try {
            $res = $this->httpRequest->request(
                'GET',
                $url,
                ['headers' => ['Accept' => 'application/json']]
            );

            if ($res->getStatusCode() == self::HTTP_REQUEST_OK) {
                if ($jsonEncode) {
                    return json_decode($res->getBody(), true);
                } else {
                    return $res->getBody();
                }
            } else if ($res->getStatusCode() == self::TOO_MANY_REQUEST) {
                // read header, wait specified time and recursivly call $this->requestApi
            } else {
                throw new Exception("HTTP Request error (status code " . $res->getStatusCode() . ")");
            }
        } catch (\Exception $e) {
            file_put_contents(__DIR__ . '/../../../http.log', $e . " : " . $url, FILE_APPEND);
            throw new Exception("Error while processing request for url " . $url);
        }
	}
	
	public function getApiConfiguration() {
	    $ApiUrl = $this->ApiUrlBase . "/3/" . "configuration" . "?" . $this->ApiKeyParam;
	    try {
	        return $this->requestApi($ApiUrl);
	    } catch (\Exception $e) {
	        throw new Exception("Error while retrieving configuration");
	    }
	}
	
    /** Retourne une collection de films pour un motif de recherche
     * @param $name
     * @return array
     * @throws Exception
     */
    public function searchMovieByName($name) {
        if ( empty($name) ) {
            throw new Exception("Name must be provided");
        }

        $ApiUrl = $this->ApiUrlBase . "/3/" . "search" . "/" . "movie" . "?query=" . $name . "&" . $this->ApiKeyParam;
        try {
            $requestedData = $this->requestApi($ApiUrl);
            $movieCollection = array();
            foreach ($requestedData['results'] as $movie) {
                if ($movie['backdrop_path'] !== null) {
                    $movieDetail = $this->getMovieById($movie['id']);
                    array_push($movieCollection, $movieDetail);
                }
            }
            return $movieCollection;
        } catch (\Exception $e) {
            throw new Exception("Error while searching data for " . $name . "(" . $e . ")");
        }
    }

    /**  Retourne une collection des films les plus populaires du moment
     * @param int $limit
     * @return array
     * @throws Exception
     */
	public function getPopularMovies($limit = 0) {
		$ApiUrl = $this->ApiUrlBase . "/3/" . "discover" . "/" . "movie" . "?sort_by=popularity.desc" . "&" . $this->ApiKeyParam;
		try {
            $requestedData = $this->requestApi($ApiUrl);
            $movieCollection = array();
            $count = 0;
            foreach ($requestedData['results'] as $movie) {
                if ($movie['backdrop_path'] !== null) {
                    $movieDetail = $this->getMovieById($movie['id']);
                    array_push($movieCollection, $movieDetail);
                }
                if ($limit>0) {
                    $count++;
                    if($count==$limit) {
                        break;
                    }
                }
            }
            return $movieCollection;
		} catch (\Exception $e) {
			throw new Exception("Error while searching for popular movies (" . $e . ")");
		}
	}

	/*
	 * Retourne un flux JSON pour les informations d'un film
	 */
	public function getMovieById($id) {
	    if (empty($id) || !is_numeric($id)) {
            throw new Exception ("\$id must be provided");
        }

        $ApiUrl = $this->ApiUrlBase . "/3/" . "movie" . "/" . $id . "?" . $this->ApiKeyParam . "&append_to_response=trailers,credits";
        
        try {
            return $this->requestApi($ApiUrl);
        } catch (\Exception $e) {
            throw new Exception("Error while retrieving data for movie id " . $id . "(" . $e . ")");
        }
	}

    /** Retourne un tableau des affiches Anglaises pour un film par ID
     * @param $id
     * @param string $size
     * @return array
     * @throws Exception
     */
	public function getMoviePostersById($id, $size="large") {
        if (empty($id) || !is_numeric($id)) {
            throw new Exception ("\$id must be provided");
        }
        
        $subDir = array();
        if (preg_match("(small|medium|large)", $size)) {
            $subDir ['small'] = 'w92';
            $subDir ['medium'] = 'w342';
            $subDir ['large'] = 'w780';
        }
    
        $ApiUrl = $this->ApiUrlBase . "/3/" . "movie" . "/" . $id . "/images?" . $this->ApiKeyParam;
        $posterEnCollection = array();
       
	    try {
           $posterCollection = $this->requestApi($ApiUrl);
	    } catch (\Exception $e) {
	        throw new Exception("Error while retrieving posters for movie id " . $id . "(" . $e . ")");
	    }
	    
	    foreach ( $posterCollection['posters'] as $poster) {
            if ( $poster['iso_639_1'] == "en" ) {
                array_push($posterEnCollection,  self::POSTER_PATH .  $subDir [$size] . $poster['file_path']);
            }
        }

        return $posterEnCollection;
	}

    /** Retourne les données brutes d'une ressource passée en paramètre
     * @param $url
     * @return mixed|\Psr\Http\Message\StreamInterface
     * @throws Exception
     */
	public function downloadPoster($url) {
	    if (empty($url)) {
            throw new Exception ("\$url must be provided");
        }

	    try {
	       return $this->requestApi($url, false);
	    } catch (Exception $e) {
	        throw new Exception("Error while retrieving data for url " . $url . "(" . $e . ")");
	    }
	}
}