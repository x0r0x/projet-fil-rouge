<?php

namespace Scrapper;
use phpFastCache\CacheManager;

class CachedScrapper
{
    const SHORT_CACHED_TIME = 1200;
    const LONG_CACHED_TIME = 86400;
    
    private $cache;
    private $scrapper;

    /**
     * CachedScrapper constructor.
     * @param Scrapper $scrapper
     */
    function __construct(Scrapper $scrapper) {
        $this->cache = CacheManager::Files();
        $this->scrapper = $scrapper; 
    }

    /**
     * @return mixed|\Psr\Http\Message\StreamInterface
     * @throws \Exception
     */
    public function getApiConfiguration() {
        $apiConfiguration = $this->cache->get('getApiConfiguration::ApiConfiguration');
        if (is_null($apiConfiguration)) {
            $apiConfiguration = $this->scrapper->getApiConfiguration();
            $this->cache->set('getApiConfiguration::ApiConfiguration', $apiConfiguration, self::LONG_CACHED_TIME);
        }
        return $apiConfiguration;
    }

    /**
     * @param $name
     * @return array|mixed
     * @throws \Exception
     */
    public function searchMovieByName($name) {
        $movies = $this->cache->get('searchMovieByName::'.$name);
        if (is_null($movies)) {
            $movies = $this->scrapper->searchMovieByName($name);
            $this->cache->set('searchMovieByName::'.$name, $movies, self::SHORT_CACHED_TIME);
        }
        return $movies;
    }

    /**
     * @param $id
     * @return mixed|\Psr\Http\Message\StreamInterface
     * @throws \Exception
     */
    public function getMovieById($id) {
        $movie = $this->cache->get('getMovieById::'.$id);
        if (is_null($movie)) {
            $movie = $this->scrapper->getMovieById($id);
            $this->cache->set('getMovieById::'.$id, $movie, self::SHORT_CACHED_TIME);
        }
        return $movie;
    }

    /**
     * @param $id
     * @param $size
     * @return array|mixed
     * @throws \Exception
     */
    public function getMoviePostersById($id, $size) {
        $posters = $this->cache->get('getMoviePostersById::'.$id);
        if (is_null($posters)) {
            $posters = $this->scrapper->getMoviePostersById($id, $size);
            $this->cache->set('getMoviePostersById::'.$id, $posters, self::SHORT_CACHED_TIME);
        }
        return $posters;
    }

    /**
     * @param $limit
     * @return array|mixed
     * @throws \Exception
     */
    public function getPopularMovies($limit) {
        $popularMovies = $this->cache->get('getPopularMovies::popularMoviesList');
        if (is_null($popularMovies)) {
            $popularMovies = $this->scrapper->getPopularMovies($limit);
            $this->cache->set('getPopularMovies::popularMoviesList', $popularMovies, self::SHORT_CACHED_TIME);
        }
        return $popularMovies;
    }

    /**
     * @param $url
     * @return mixed|\Psr\Http\Message\StreamInterface
     * @throws \Exception
     */
    public function downloadPoster($url) {
        return $this->scrapper->downloadPoster($url);
    }
} 