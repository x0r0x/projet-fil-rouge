<?php

namespace Library;

use Base;
use Propel\Runtime\Exception\EntityNotFoundException;

class Library
{
	private $movieDetail;

	/**
	 * @return \Director|null
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	private function saveDirector() {
		$crew = $this->movieDetail['credits']['crew'];
		foreach ($crew as $key => $value) {
			if ($value['job'] == 'Director') {
				$fullName = explode(" ", $value['name']);

                (isset($fullName[0])) ? $firstName = $fullName[0] : $firstName = '';
				(isset($fullName[1])) ? $lastName = $fullName[1] : $lastName = '';

				$movieDirector = Base\DirectorQuery::create()
					->filterByFirstname($firstName)
					->filterByLastname($lastName)
					->findOneOrCreate();

				if ($movieDirector->isNew()) {
					$movieDirector->save();
				}

				return $movieDirector;
			}
		}
        return null;
	}

	/**
	 * @param $title
	 * @param $year
	 * @param $movieDirector
	 * @param $posterUrl
	 * @param $externalId
	 * @return \Movie
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	private function saveMovie($title, $year, $movieDirector, $posterUrl, $externalId) {

        $movie = Base\MovieQuery::create()
            ->filterByTitle($title)
            ->filterByYear($year)
            ->filterByDirector($movieDirector)
            ->findOneOrCreate();

		if ($movie->isNew()) {
			(isset($this->movieDetail['backdrop_path'])) ? $backdrop = "http://image.tmdb.org/t/p/w300" . $this->movieDetail['backdrop_path'] : $backdrop = '';
			(isset($this->movieDetail['vote_average'])) ? $rating = $this->movieDetail['vote_average'] : $rating = '';
			(isset($this->movieDetail['overview'])) ? $synopsis = $this->movieDetail['overview'] : $synopsis = '';
			(isset($this->movieDetail['trailers']['youtube'][0]['source'])) ? $trailer = $this->movieDetail['trailers']['youtube'][0]['source'] : $trailer = '';
			(isset($this->movieDetail['runtime'])) ? $runtime = $this->movieDetail['runtime'] : $runtime = '';
			(isset($this->movieDetail['adult'])) ? $adult = $this->movieDetail['adult'] : $adult = '';

			$movie->setPoster(basename($posterUrl));
			$movie->setBackdrop(basename($backdrop));
			$movie->setRating($rating);
			$movie->setSynopsis($this->movieDetail['overview']);
			$movie->setTrailer($trailer);
			$movie->setRuntime($runtime);
			$movie->setAdult($adult);
			$movie->setId($externalId);
  			$movie->save();
		}
		return $movie;
	}

	/**
	 * @param array $movieDetail
	 * @param $posterUrl
	 */
	public function saveToLibrary(array $movieDetail, $posterUrl) {
		$this->movieDetail = $movieDetail;
		$title = $this->movieDetail['title'];
		$year = substr($this->movieDetail['release_date'], 0, 4);
		$movieDirector = $this->saveDirector();
        if ($movieDirector !== null)
        {
            $movie = $this->saveMovie($title, $year, $movieDirector, $posterUrl, $this->movieDetail['id']);
            $this->saveCategory($title, $movie);
            $this->saveStudioProduction($title, $movie);
            $this->saveProduction($title, $movie);
        }
	}

	/**
	 * @param $title
	 * @param $movie
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	private function saveCategory($title, $movie) {
		$category = $this->movieDetail['genres'];
		foreach ($category as $key => $value) {
			$movieCategory = Base\CategoryQuery::create()
				->filterByName($value['name'])
				->findOneOrCreate();

			if ($movieCategory->isNew()) {
  				$movieCategory->save();
			}

			$movieLinkCategory = Base\MovieLinkCategoryQuery::create()
				->filterByMovie($movie)
				->filterByCategoryName($value['name'])
				->findOneOrCreate();
	
			if ($movieLinkCategory->isNew()) {
				$movieLinkCategory->setMovieTitle($title);
				$movieLinkCategory->setCategoryName($value['name']);
  				$movieLinkCategory->save();
			}
		}
	}

	/**
	 * @param $title
	 * @param $movie
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	private function saveStudioProduction($title, $movie) {
		$production = $this->movieDetail['production_companies'];
		foreach ($production as $key => $value) {
			$movieStudio = Base\StudioQuery::create()
				->filterByName($value['name'])
				->findOneOrCreate();

			if ($movieStudio->isNew()) {
				$productionId = Base\ProductionQuery::create()
					->filterByStudio($movieStudio)
					->findOneOrCreate();
				$productionId->save();
				$linkId = $productionId->getId();
				$movieStudio->setProductionId($linkId);
  				$movieStudio->save();
			} else {
				$linkId = $movieStudio->getProductionId();
			}

			$movieLinkProduction = Base\MovieLinkProductionQuery::create()
				->filterByMovie($movie)
				->filterByProductionId($linkId)
				->findOneOrCreate();
	
			if ($movieLinkProduction->isNew()) {
				$movieLinkProduction->setMovieTitle($title);
				$movieLinkProduction->setProductionId($linkId);
  				$movieLinkProduction->save();
			}
		}
	}

	/**
	 * @param $title
	 * @param $movie
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	private function saveProduction($title, $movie) {
		$crew = $this->movieDetail['credits']['crew'];
		foreach ($crew as $key => $value) {
			if ($value['job'] == 'Producer') {
				$fullName = explode(" ", $value['name']);
				$firstName = $fullName[0];
				$lastName = $fullName[1];

				$movieProducer = Base\ProducerQuery::create()
					->filterByFirstname($firstName)
					->filterByLastname($lastName)
					->findOneOrCreate();

				if ($movieProducer->isNew()) {
					$productionId = Base\ProductionQuery::create()
						->filterByProducer($movieProducer)
						->findOneOrCreate();
					$productionId->save();
					$linkId = $productionId->getId();
					$movieProducer->setProductionId($linkId);
	  				$movieProducer->save();
				} else {
					$linkId = $movieProducer->getProductionId();
				}

				$movieLinkProduction = Base\MovieLinkProductionQuery::create()
					->filterByMovie($movie)
					->filterByProductionId($linkId)
					->findOneOrCreate();
		
				if ($movieLinkProduction->isNew()) {
					$movieLinkProduction->setMovieTitle($title);
					$movieLinkProduction->setProductionId($linkId);
	  				$movieLinkProduction->save();
				}
			}
		}
	}

	/**
	 * @param $title
	 * @return array
	 */
	public function findMovieByTitle($title) {
        return Base\MovieQuery::create()->setIgnoreCase(true)->filterByTitle('%'.$title.'%')->find()->toArray();
	}

	/**
	 * @param $id
	 * @return \Movie|null
	 */
	public function findMovieById($id) {
		$movie = null;
			try {
				$movie = Base\MovieQuery::create()->setIgnoreCase(true)-> requireOneById($id);
			} catch (EntityNotFoundException $e) {
				return null;
			}
		return $movie->toArray();
	}

	/**
	 * @param $title
	 * @return array
	 */
    public function findCategoryByMovieTitle($title) {
        return Base\MovieLinkCategoryQuery::create()->setIgnoreCase(true)->filterByMovieTitle($title)->find()->toArray();
    }

	/**
	 * @param $id
	 * @return \Director|null
	 */
	public function findDirectorById($id)
	{
		return Base\DirectorQuery::create()->setIgnoreCase(true)->findOneById($id);
	}

	/**
	 * @param int $limit
	 * @return array
	 */
	public function findPopularMovies($limit = 0) {
		// Les données sont statiques pour le moment
		$iDlist = array(293660, 246655, 269149, 271110, 205584, 100402, 140607, 368596, 267860, 299687, 384798, 392536, 273248, 300673, 241259, 146198, 321741, 300671, 339547, 321258);
		$movieCollection = array();
		if ($limit === 0) {
			$limit = count($iDlist);
		}
		$i = 0;
		foreach ($iDlist as $id) {
			if ($i >= $limit) {
				return $movieCollection;
			}
			$i++;
			$movie = $this->findMovieById($id);
			if ($movie !== null) {
				array_push($movieCollection, $movie);
			}
		}
		return $movieCollection;
	}
}
