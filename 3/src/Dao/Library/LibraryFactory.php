<?php

namespace Library;

use Propel;

class LibraryFactory
{

	public static function setupOrm()
	{
		$localDsnFileName = __DIR__ . '/../../mysql-dsn.conf';

		if (!file_exists($localDsnFileName))
		{
			$localDsnFH = fopen($localDsnFileName, 'w');
			fputs($localDsnFH, 'mysql:host=127.0.0.1;dbname=VODB'); // Write default value
			fclose($localDsnFH);
		}
		$localDsnFH = fopen($localDsnFileName, 'r');
		$localDsn = fgets($localDsnFH, 255);
		fclose($localDsnFH);

		$serviceContainer = Propel\Runtime\Propel::getServiceContainer();
		$serviceContainer->checkVersion('2.0.0-dev');
		$serviceContainer->setAdapterClass('VODB', 'mysql');
		$manager = new Propel\Runtime\Connection\ConnectionManagerSingle();
		$manager->setConfiguration(array (
			'classname' => 'Propel\\Runtime\\Connection\\ConnectionWrapper',
			'dsn' => $localDsn,
			'user' => 'scott',
			'password' => 'tiger',
			'attributes' =>
				array (
					'ATTR_EMULATE_PREPARES' => false,
				),
			'settings' =>
				array (
					'charset' => 'utf8',
					'queries' =>
						array (
							'utf8' => 'SET NAMES utf8 COLLATE utf8_general_ci, COLLATION_CONNECTION = utf8_general_ci, COLLATION_DATABASE = utf8_general_ci, COLLATION_SERVER = utf8_general_ci',
						),
				),
		));
		$manager->setName('VODB');
		$serviceContainer->setConnectionManager('VODB', $manager);
		$serviceContainer->setDefaultDatasource('VODB');
	}

	/**
	 * @return Library
	 */
	public static function getInstance()
	{
		self::setupOrm();
		return new Library();
	}
}