<?php

use Base\Production as BaseProduction;

/**
 * Skeleton subclass for representing a row from the 'production' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Production extends BaseProduction
{

}
