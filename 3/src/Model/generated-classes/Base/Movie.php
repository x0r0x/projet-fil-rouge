<?php

namespace Base;

use \Cast as ChildCast;
use \CastQuery as ChildCastQuery;
use \Director as ChildDirector;
use \DirectorQuery as ChildDirectorQuery;
use \Movie as ChildMovie;
use \MovieLinkCategory as ChildMovieLinkCategory;
use \MovieLinkCategoryQuery as ChildMovieLinkCategoryQuery;
use \MovieLinkProduction as ChildMovieLinkProduction;
use \MovieLinkProductionQuery as ChildMovieLinkProductionQuery;
use \MovieQuery as ChildMovieQuery;
use \Exception;
use \PDO;
use Map\MovieTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'movie' table.
 *
 *
 *
* @package    propel.generator..Base
*/
abstract class Movie implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\MovieTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the title field.
     * @var        string
     */
    protected $title;

    /**
     * The value for the year field.
     * @var        int
     */
    protected $year;

    /**
     * The value for the director_id field.
     * @var        int
     */
    protected $director_id;

    /**
     * The value for the poster field.
     * @var        string
     */
    protected $poster;

    /**
     * The value for the backdrop field.
     * @var        string
     */
    protected $backdrop;

    /**
     * The value for the rating field.
     * @var        double
     */
    protected $rating;

    /**
     * The value for the synopsis field.
     * @var        string
     */
    protected $synopsis;

    /**
     * The value for the trailer field.
     * @var        string
     */
    protected $trailer;

    /**
     * The value for the runtime field.
     * @var        int
     */
    protected $runtime;

    /**
     * The value for the adult field.
     * @var        boolean
     */
    protected $adult;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * @var        ChildDirector
     */
    protected $aDirector;

    /**
     * @var        ObjectCollection|ChildCast[] Collection to store aggregation of ChildCast objects.
     */
    protected $collCasts;
    protected $collCastsPartial;

    /**
     * @var        ObjectCollection|ChildMovieLinkCategory[] Collection to store aggregation of ChildMovieLinkCategory objects.
     */
    protected $collMovieLinkCategories;
    protected $collMovieLinkCategoriesPartial;

    /**
     * @var        ObjectCollection|ChildMovieLinkProduction[] Collection to store aggregation of ChildMovieLinkProduction objects.
     */
    protected $collMovieLinkProductions;
    protected $collMovieLinkProductionsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCast[]
     */
    protected $castsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMovieLinkCategory[]
     */
    protected $movieLinkCategoriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMovieLinkProduction[]
     */
    protected $movieLinkProductionsScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\Movie object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Movie</code> instance.  If
     * <code>obj</code> is an instance of <code>Movie</code>, delegates to
     * <code>equals(Movie)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Movie The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        return array_keys(get_object_vars($this));
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [year] column value.
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Get the [director_id] column value.
     *
     * @return int
     */
    public function getDirectorId()
    {
        return $this->director_id;
    }

    /**
     * Get the [poster] column value.
     *
     * @return string
     */
    public function getPoster()
    {
        return $this->poster;
    }

    /**
     * Get the [backdrop] column value.
     *
     * @return string
     */
    public function getBackdrop()
    {
        return $this->backdrop;
    }

    /**
     * Get the [rating] column value.
     *
     * @return double
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Get the [synopsis] column value.
     *
     * @return string
     */
    public function getSynopsis()
    {
        return $this->synopsis;
    }

    /**
     * Get the [trailer] column value.
     *
     * @return string
     */
    public function getTrailer()
    {
        return $this->trailer;
    }

    /**
     * Get the [runtime] column value.
     *
     * @return int
     */
    public function getRuntime()
    {
        return $this->runtime;
    }

    /**
     * Get the [adult] column value.
     *
     * @return boolean
     */
    public function getAdult()
    {
        return $this->adult;
    }

    /**
     * Get the [adult] column value.
     *
     * @return boolean
     */
    public function isAdult()
    {
        return $this->getAdult();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of [title] column.
     *
     * @param string $v new value
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[MovieTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [year] column.
     *
     * @param int $v new value
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function setYear($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->year !== $v) {
            $this->year = $v;
            $this->modifiedColumns[MovieTableMap::COL_YEAR] = true;
        }

        return $this;
    } // setYear()

    /**
     * Set the value of [director_id] column.
     *
     * @param int $v new value
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function setDirectorId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->director_id !== $v) {
            $this->director_id = $v;
            $this->modifiedColumns[MovieTableMap::COL_DIRECTOR_ID] = true;
        }

        if ($this->aDirector !== null && $this->aDirector->getId() !== $v) {
            $this->aDirector = null;
        }

        return $this;
    } // setDirectorId()

    /**
     * Set the value of [poster] column.
     *
     * @param string $v new value
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function setPoster($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->poster !== $v) {
            $this->poster = $v;
            $this->modifiedColumns[MovieTableMap::COL_POSTER] = true;
        }

        return $this;
    } // setPoster()

    /**
     * Set the value of [backdrop] column.
     *
     * @param string $v new value
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function setBackdrop($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->backdrop !== $v) {
            $this->backdrop = $v;
            $this->modifiedColumns[MovieTableMap::COL_BACKDROP] = true;
        }

        return $this;
    } // setBackdrop()

    /**
     * Set the value of [rating] column.
     *
     * @param double $v new value
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function setRating($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->rating !== $v) {
            $this->rating = $v;
            $this->modifiedColumns[MovieTableMap::COL_RATING] = true;
        }

        return $this;
    } // setRating()

    /**
     * Set the value of [synopsis] column.
     *
     * @param string $v new value
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function setSynopsis($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->synopsis !== $v) {
            $this->synopsis = $v;
            $this->modifiedColumns[MovieTableMap::COL_SYNOPSIS] = true;
        }

        return $this;
    } // setSynopsis()

    /**
     * Set the value of [trailer] column.
     *
     * @param string $v new value
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function setTrailer($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->trailer !== $v) {
            $this->trailer = $v;
            $this->modifiedColumns[MovieTableMap::COL_TRAILER] = true;
        }

        return $this;
    } // setTrailer()

    /**
     * Set the value of [runtime] column.
     *
     * @param int $v new value
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function setRuntime($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->runtime !== $v) {
            $this->runtime = $v;
            $this->modifiedColumns[MovieTableMap::COL_RUNTIME] = true;
        }

        return $this;
    } // setRuntime()

    /**
     * Sets the value of the [adult] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function setAdult($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->adult !== $v) {
            $this->adult = $v;
            $this->modifiedColumns[MovieTableMap::COL_ADULT] = true;
        }

        return $this;
    } // setAdult()

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[MovieTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : MovieTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : MovieTableMap::translateFieldName('Year', TableMap::TYPE_PHPNAME, $indexType)];
            $this->year = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : MovieTableMap::translateFieldName('DirectorId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->director_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : MovieTableMap::translateFieldName('Poster', TableMap::TYPE_PHPNAME, $indexType)];
            $this->poster = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : MovieTableMap::translateFieldName('Backdrop', TableMap::TYPE_PHPNAME, $indexType)];
            $this->backdrop = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : MovieTableMap::translateFieldName('Rating', TableMap::TYPE_PHPNAME, $indexType)];
            $this->rating = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : MovieTableMap::translateFieldName('Synopsis', TableMap::TYPE_PHPNAME, $indexType)];
            $this->synopsis = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : MovieTableMap::translateFieldName('Trailer', TableMap::TYPE_PHPNAME, $indexType)];
            $this->trailer = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : MovieTableMap::translateFieldName('Runtime', TableMap::TYPE_PHPNAME, $indexType)];
            $this->runtime = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : MovieTableMap::translateFieldName('Adult', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adult = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : MovieTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 11; // 11 = MovieTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Movie'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aDirector !== null && $this->director_id !== $this->aDirector->getId()) {
            $this->aDirector = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MovieTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildMovieQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aDirector = null;
            $this->collCasts = null;

            $this->collMovieLinkCategories = null;

            $this->collMovieLinkProductions = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Movie::setDeleted()
     * @see Movie::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovieTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildMovieQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovieTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $isInsert = $this->isNew();
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MovieTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aDirector !== null) {
                if ($this->aDirector->isModified() || $this->aDirector->isNew()) {
                    $affectedRows += $this->aDirector->save($con);
                }
                $this->setDirector($this->aDirector);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->castsScheduledForDeletion !== null) {
                if (!$this->castsScheduledForDeletion->isEmpty()) {
                    \CastQuery::create()
                        ->filterByPrimaryKeys($this->castsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->castsScheduledForDeletion = null;
                }
            }

            if ($this->collCasts !== null) {
                foreach ($this->collCasts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->movieLinkCategoriesScheduledForDeletion !== null) {
                if (!$this->movieLinkCategoriesScheduledForDeletion->isEmpty()) {
                    \MovieLinkCategoryQuery::create()
                        ->filterByPrimaryKeys($this->movieLinkCategoriesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->movieLinkCategoriesScheduledForDeletion = null;
                }
            }

            if ($this->collMovieLinkCategories !== null) {
                foreach ($this->collMovieLinkCategories as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->movieLinkProductionsScheduledForDeletion !== null) {
                if (!$this->movieLinkProductionsScheduledForDeletion->isEmpty()) {
                    \MovieLinkProductionQuery::create()
                        ->filterByPrimaryKeys($this->movieLinkProductionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->movieLinkProductionsScheduledForDeletion = null;
                }
            }

            if ($this->collMovieLinkProductions !== null) {
                foreach ($this->collMovieLinkProductions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(MovieTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(MovieTableMap::COL_YEAR)) {
            $modifiedColumns[':p' . $index++]  = 'year';
        }
        if ($this->isColumnModified(MovieTableMap::COL_DIRECTOR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'director_id';
        }
        if ($this->isColumnModified(MovieTableMap::COL_POSTER)) {
            $modifiedColumns[':p' . $index++]  = 'poster';
        }
        if ($this->isColumnModified(MovieTableMap::COL_BACKDROP)) {
            $modifiedColumns[':p' . $index++]  = 'backdrop';
        }
        if ($this->isColumnModified(MovieTableMap::COL_RATING)) {
            $modifiedColumns[':p' . $index++]  = 'rating';
        }
        if ($this->isColumnModified(MovieTableMap::COL_SYNOPSIS)) {
            $modifiedColumns[':p' . $index++]  = 'synopsis';
        }
        if ($this->isColumnModified(MovieTableMap::COL_TRAILER)) {
            $modifiedColumns[':p' . $index++]  = 'trailer';
        }
        if ($this->isColumnModified(MovieTableMap::COL_RUNTIME)) {
            $modifiedColumns[':p' . $index++]  = 'runtime';
        }
        if ($this->isColumnModified(MovieTableMap::COL_ADULT)) {
            $modifiedColumns[':p' . $index++]  = 'adult';
        }
        if ($this->isColumnModified(MovieTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }

        $sql = sprintf(
            'INSERT INTO movie (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'year':
                        $stmt->bindValue($identifier, $this->year, PDO::PARAM_INT);
                        break;
                    case 'director_id':
                        $stmt->bindValue($identifier, $this->director_id, PDO::PARAM_INT);
                        break;
                    case 'poster':
                        $stmt->bindValue($identifier, $this->poster, PDO::PARAM_STR);
                        break;
                    case 'backdrop':
                        $stmt->bindValue($identifier, $this->backdrop, PDO::PARAM_STR);
                        break;
                    case 'rating':
                        $stmt->bindValue($identifier, $this->rating, PDO::PARAM_STR);
                        break;
                    case 'synopsis':
                        $stmt->bindValue($identifier, $this->synopsis, PDO::PARAM_STR);
                        break;
                    case 'trailer':
                        $stmt->bindValue($identifier, $this->trailer, PDO::PARAM_STR);
                        break;
                    case 'runtime':
                        $stmt->bindValue($identifier, $this->runtime, PDO::PARAM_INT);
                        break;
                    case 'adult':
                        $stmt->bindValue($identifier, (int) $this->adult, PDO::PARAM_INT);
                        break;
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MovieTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getTitle();
                break;
            case 1:
                return $this->getYear();
                break;
            case 2:
                return $this->getDirectorId();
                break;
            case 3:
                return $this->getPoster();
                break;
            case 4:
                return $this->getBackdrop();
                break;
            case 5:
                return $this->getRating();
                break;
            case 6:
                return $this->getSynopsis();
                break;
            case 7:
                return $this->getTrailer();
                break;
            case 8:
                return $this->getRuntime();
                break;
            case 9:
                return $this->getAdult();
                break;
            case 10:
                return $this->getId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Movie'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Movie'][$this->hashCode()] = true;
        $keys = MovieTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getTitle(),
            $keys[1] => $this->getYear(),
            $keys[2] => $this->getDirectorId(),
            $keys[3] => $this->getPoster(),
            $keys[4] => $this->getBackdrop(),
            $keys[5] => $this->getRating(),
            $keys[6] => $this->getSynopsis(),
            $keys[7] => $this->getTrailer(),
            $keys[8] => $this->getRuntime(),
            $keys[9] => $this->getAdult(),
            $keys[10] => $this->getId(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aDirector) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'director';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'director';
                        break;
                    default:
                        $key = 'Director';
                }

                $result[$key] = $this->aDirector->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCasts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'casts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'casts';
                        break;
                    default:
                        $key = 'Casts';
                }

                $result[$key] = $this->collCasts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMovieLinkCategories) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'movieLinkCategories';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'movielinkcategories';
                        break;
                    default:
                        $key = 'MovieLinkCategories';
                }

                $result[$key] = $this->collMovieLinkCategories->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMovieLinkProductions) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'movieLinkProductions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'movielinkproductions';
                        break;
                    default:
                        $key = 'MovieLinkProductions';
                }

                $result[$key] = $this->collMovieLinkProductions->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Movie
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MovieTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Movie
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setTitle($value);
                break;
            case 1:
                $this->setYear($value);
                break;
            case 2:
                $this->setDirectorId($value);
                break;
            case 3:
                $this->setPoster($value);
                break;
            case 4:
                $this->setBackdrop($value);
                break;
            case 5:
                $this->setRating($value);
                break;
            case 6:
                $this->setSynopsis($value);
                break;
            case 7:
                $this->setTrailer($value);
                break;
            case 8:
                $this->setRuntime($value);
                break;
            case 9:
                $this->setAdult($value);
                break;
            case 10:
                $this->setId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = MovieTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setTitle($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setYear($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setDirectorId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setPoster($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setBackdrop($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setRating($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setSynopsis($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setTrailer($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setRuntime($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setAdult($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setId($arr[$keys[10]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Movie The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(MovieTableMap::DATABASE_NAME);

        if ($this->isColumnModified(MovieTableMap::COL_TITLE)) {
            $criteria->add(MovieTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(MovieTableMap::COL_YEAR)) {
            $criteria->add(MovieTableMap::COL_YEAR, $this->year);
        }
        if ($this->isColumnModified(MovieTableMap::COL_DIRECTOR_ID)) {
            $criteria->add(MovieTableMap::COL_DIRECTOR_ID, $this->director_id);
        }
        if ($this->isColumnModified(MovieTableMap::COL_POSTER)) {
            $criteria->add(MovieTableMap::COL_POSTER, $this->poster);
        }
        if ($this->isColumnModified(MovieTableMap::COL_BACKDROP)) {
            $criteria->add(MovieTableMap::COL_BACKDROP, $this->backdrop);
        }
        if ($this->isColumnModified(MovieTableMap::COL_RATING)) {
            $criteria->add(MovieTableMap::COL_RATING, $this->rating);
        }
        if ($this->isColumnModified(MovieTableMap::COL_SYNOPSIS)) {
            $criteria->add(MovieTableMap::COL_SYNOPSIS, $this->synopsis);
        }
        if ($this->isColumnModified(MovieTableMap::COL_TRAILER)) {
            $criteria->add(MovieTableMap::COL_TRAILER, $this->trailer);
        }
        if ($this->isColumnModified(MovieTableMap::COL_RUNTIME)) {
            $criteria->add(MovieTableMap::COL_RUNTIME, $this->runtime);
        }
        if ($this->isColumnModified(MovieTableMap::COL_ADULT)) {
            $criteria->add(MovieTableMap::COL_ADULT, $this->adult);
        }
        if ($this->isColumnModified(MovieTableMap::COL_ID)) {
            $criteria->add(MovieTableMap::COL_ID, $this->id);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildMovieQuery::create();
        $criteria->add(MovieTableMap::COL_TITLE, $this->title);
        $criteria->add(MovieTableMap::COL_YEAR, $this->year);
        $criteria->add(MovieTableMap::COL_DIRECTOR_ID, $this->director_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getTitle() &&
            null !== $this->getYear() &&
            null !== $this->getDirectorId();

        $validPrimaryKeyFKs = 1;
        $primaryKeyFKs = [];

        //relation movie_fk_2cc047 to table director
        if ($this->aDirector && $hash = spl_object_hash($this->aDirector)) {
            $primaryKeyFKs[] = $hash;
        } else {
            $validPrimaryKeyFKs = false;
        }

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getTitle();
        $pks[1] = $this->getYear();
        $pks[2] = $this->getDirectorId();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param      array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setTitle($keys[0]);
        $this->setYear($keys[1]);
        $this->setDirectorId($keys[2]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return (null === $this->getTitle()) && (null === $this->getYear()) && (null === $this->getDirectorId());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Movie (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTitle($this->getTitle());
        $copyObj->setYear($this->getYear());
        $copyObj->setDirectorId($this->getDirectorId());
        $copyObj->setPoster($this->getPoster());
        $copyObj->setBackdrop($this->getBackdrop());
        $copyObj->setRating($this->getRating());
        $copyObj->setSynopsis($this->getSynopsis());
        $copyObj->setTrailer($this->getTrailer());
        $copyObj->setRuntime($this->getRuntime());
        $copyObj->setAdult($this->getAdult());
        $copyObj->setId($this->getId());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCasts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCast($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMovieLinkCategories() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMovieLinkCategory($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMovieLinkProductions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMovieLinkProduction($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Movie Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildDirector object.
     *
     * @param  ChildDirector $v
     * @return $this|\Movie The current object (for fluent API support)
     * @throws PropelException
     */
    public function setDirector(ChildDirector $v = null)
    {
        if ($v === null) {
            $this->setDirectorId(NULL);
        } else {
            $this->setDirectorId($v->getId());
        }

        $this->aDirector = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildDirector object, it will not be re-added.
        if ($v !== null) {
            $v->addMovie($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildDirector object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildDirector The associated ChildDirector object.
     * @throws PropelException
     */
    public function getDirector(ConnectionInterface $con = null)
    {
        if ($this->aDirector === null && ($this->director_id !== null)) {
            $this->aDirector = ChildDirectorQuery::create()->findPk($this->director_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aDirector->addMovies($this);
             */
        }

        return $this->aDirector;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Cast' == $relationName) {
            return $this->initCasts();
        }
        if ('MovieLinkCategory' == $relationName) {
            return $this->initMovieLinkCategories();
        }
        if ('MovieLinkProduction' == $relationName) {
            return $this->initMovieLinkProductions();
        }
    }

    /**
     * Clears out the collCasts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCasts()
     */
    public function clearCasts()
    {
        $this->collCasts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCasts collection loaded partially.
     */
    public function resetPartialCasts($v = true)
    {
        $this->collCastsPartial = $v;
    }

    /**
     * Initializes the collCasts collection.
     *
     * By default this just sets the collCasts collection to an empty array (like clearcollCasts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCasts($overrideExisting = true)
    {
        if (null !== $this->collCasts && !$overrideExisting) {
            return;
        }
        $this->collCasts = new ObjectCollection();
        $this->collCasts->setModel('\Cast');
    }

    /**
     * Gets an array of ChildCast objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMovie is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCast[] List of ChildCast objects
     * @throws PropelException
     */
    public function getCasts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCastsPartial && !$this->isNew();
        if (null === $this->collCasts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCasts) {
                // return empty collection
                $this->initCasts();
            } else {
                $collCasts = ChildCastQuery::create(null, $criteria)
                    ->filterByMovie($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCastsPartial && count($collCasts)) {
                        $this->initCasts(false);

                        foreach ($collCasts as $obj) {
                            if (false == $this->collCasts->contains($obj)) {
                                $this->collCasts->append($obj);
                            }
                        }

                        $this->collCastsPartial = true;
                    }

                    return $collCasts;
                }

                if ($partial && $this->collCasts) {
                    foreach ($this->collCasts as $obj) {
                        if ($obj->isNew()) {
                            $collCasts[] = $obj;
                        }
                    }
                }

                $this->collCasts = $collCasts;
                $this->collCastsPartial = false;
            }
        }

        return $this->collCasts;
    }

    /**
     * Sets a collection of ChildCast objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $casts A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMovie The current object (for fluent API support)
     */
    public function setCasts(Collection $casts, ConnectionInterface $con = null)
    {
        /** @var ChildCast[] $castsToDelete */
        $castsToDelete = $this->getCasts(new Criteria(), $con)->diff($casts);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->castsScheduledForDeletion = clone $castsToDelete;

        foreach ($castsToDelete as $castRemoved) {
            $castRemoved->setMovie(null);
        }

        $this->collCasts = null;
        foreach ($casts as $cast) {
            $this->addCast($cast);
        }

        $this->collCasts = $casts;
        $this->collCastsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Cast objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Cast objects.
     * @throws PropelException
     */
    public function countCasts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCastsPartial && !$this->isNew();
        if (null === $this->collCasts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCasts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCasts());
            }

            $query = ChildCastQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMovie($this)
                ->count($con);
        }

        return count($this->collCasts);
    }

    /**
     * Method called to associate a ChildCast object to this object
     * through the ChildCast foreign key attribute.
     *
     * @param  ChildCast $l ChildCast
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function addCast(ChildCast $l)
    {
        if ($this->collCasts === null) {
            $this->initCasts();
            $this->collCastsPartial = true;
        }

        if (!$this->collCasts->contains($l)) {
            $this->doAddCast($l);
        }

        return $this;
    }

    /**
     * @param ChildCast $cast The ChildCast object to add.
     */
    protected function doAddCast(ChildCast $cast)
    {
        $this->collCasts[]= $cast;
        $cast->setMovie($this);
    }

    /**
     * @param  ChildCast $cast The ChildCast object to remove.
     * @return $this|ChildMovie The current object (for fluent API support)
     */
    public function removeCast(ChildCast $cast)
    {
        if ($this->getCasts()->contains($cast)) {
            $pos = $this->collCasts->search($cast);
            $this->collCasts->remove($pos);
            if (null === $this->castsScheduledForDeletion) {
                $this->castsScheduledForDeletion = clone $this->collCasts;
                $this->castsScheduledForDeletion->clear();
            }
            $this->castsScheduledForDeletion[]= clone $cast;
            $cast->setMovie(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Movie is new, it will return
     * an empty collection; or if this Movie has previously
     * been saved, it will retrieve related Casts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Movie.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCast[] List of ChildCast objects
     */
    public function getCastsJoinActor(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCastQuery::create(null, $criteria);
        $query->joinWith('Actor', $joinBehavior);

        return $this->getCasts($query, $con);
    }

    /**
     * Clears out the collMovieLinkCategories collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMovieLinkCategories()
     */
    public function clearMovieLinkCategories()
    {
        $this->collMovieLinkCategories = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMovieLinkCategories collection loaded partially.
     */
    public function resetPartialMovieLinkCategories($v = true)
    {
        $this->collMovieLinkCategoriesPartial = $v;
    }

    /**
     * Initializes the collMovieLinkCategories collection.
     *
     * By default this just sets the collMovieLinkCategories collection to an empty array (like clearcollMovieLinkCategories());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMovieLinkCategories($overrideExisting = true)
    {
        if (null !== $this->collMovieLinkCategories && !$overrideExisting) {
            return;
        }
        $this->collMovieLinkCategories = new ObjectCollection();
        $this->collMovieLinkCategories->setModel('\MovieLinkCategory');
    }

    /**
     * Gets an array of ChildMovieLinkCategory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMovie is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMovieLinkCategory[] List of ChildMovieLinkCategory objects
     * @throws PropelException
     */
    public function getMovieLinkCategories(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMovieLinkCategoriesPartial && !$this->isNew();
        if (null === $this->collMovieLinkCategories || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMovieLinkCategories) {
                // return empty collection
                $this->initMovieLinkCategories();
            } else {
                $collMovieLinkCategories = ChildMovieLinkCategoryQuery::create(null, $criteria)
                    ->filterByMovie($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMovieLinkCategoriesPartial && count($collMovieLinkCategories)) {
                        $this->initMovieLinkCategories(false);

                        foreach ($collMovieLinkCategories as $obj) {
                            if (false == $this->collMovieLinkCategories->contains($obj)) {
                                $this->collMovieLinkCategories->append($obj);
                            }
                        }

                        $this->collMovieLinkCategoriesPartial = true;
                    }

                    return $collMovieLinkCategories;
                }

                if ($partial && $this->collMovieLinkCategories) {
                    foreach ($this->collMovieLinkCategories as $obj) {
                        if ($obj->isNew()) {
                            $collMovieLinkCategories[] = $obj;
                        }
                    }
                }

                $this->collMovieLinkCategories = $collMovieLinkCategories;
                $this->collMovieLinkCategoriesPartial = false;
            }
        }

        return $this->collMovieLinkCategories;
    }

    /**
     * Sets a collection of ChildMovieLinkCategory objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $movieLinkCategories A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMovie The current object (for fluent API support)
     */
    public function setMovieLinkCategories(Collection $movieLinkCategories, ConnectionInterface $con = null)
    {
        /** @var ChildMovieLinkCategory[] $movieLinkCategoriesToDelete */
        $movieLinkCategoriesToDelete = $this->getMovieLinkCategories(new Criteria(), $con)->diff($movieLinkCategories);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->movieLinkCategoriesScheduledForDeletion = clone $movieLinkCategoriesToDelete;

        foreach ($movieLinkCategoriesToDelete as $movieLinkCategoryRemoved) {
            $movieLinkCategoryRemoved->setMovie(null);
        }

        $this->collMovieLinkCategories = null;
        foreach ($movieLinkCategories as $movieLinkCategory) {
            $this->addMovieLinkCategory($movieLinkCategory);
        }

        $this->collMovieLinkCategories = $movieLinkCategories;
        $this->collMovieLinkCategoriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MovieLinkCategory objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MovieLinkCategory objects.
     * @throws PropelException
     */
    public function countMovieLinkCategories(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMovieLinkCategoriesPartial && !$this->isNew();
        if (null === $this->collMovieLinkCategories || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMovieLinkCategories) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMovieLinkCategories());
            }

            $query = ChildMovieLinkCategoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMovie($this)
                ->count($con);
        }

        return count($this->collMovieLinkCategories);
    }

    /**
     * Method called to associate a ChildMovieLinkCategory object to this object
     * through the ChildMovieLinkCategory foreign key attribute.
     *
     * @param  ChildMovieLinkCategory $l ChildMovieLinkCategory
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function addMovieLinkCategory(ChildMovieLinkCategory $l)
    {
        if ($this->collMovieLinkCategories === null) {
            $this->initMovieLinkCategories();
            $this->collMovieLinkCategoriesPartial = true;
        }

        if (!$this->collMovieLinkCategories->contains($l)) {
            $this->doAddMovieLinkCategory($l);
        }

        return $this;
    }

    /**
     * @param ChildMovieLinkCategory $movieLinkCategory The ChildMovieLinkCategory object to add.
     */
    protected function doAddMovieLinkCategory(ChildMovieLinkCategory $movieLinkCategory)
    {
        $this->collMovieLinkCategories[]= $movieLinkCategory;
        $movieLinkCategory->setMovie($this);
    }

    /**
     * @param  ChildMovieLinkCategory $movieLinkCategory The ChildMovieLinkCategory object to remove.
     * @return $this|ChildMovie The current object (for fluent API support)
     */
    public function removeMovieLinkCategory(ChildMovieLinkCategory $movieLinkCategory)
    {
        if ($this->getMovieLinkCategories()->contains($movieLinkCategory)) {
            $pos = $this->collMovieLinkCategories->search($movieLinkCategory);
            $this->collMovieLinkCategories->remove($pos);
            if (null === $this->movieLinkCategoriesScheduledForDeletion) {
                $this->movieLinkCategoriesScheduledForDeletion = clone $this->collMovieLinkCategories;
                $this->movieLinkCategoriesScheduledForDeletion->clear();
            }
            $this->movieLinkCategoriesScheduledForDeletion[]= clone $movieLinkCategory;
            $movieLinkCategory->setMovie(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Movie is new, it will return
     * an empty collection; or if this Movie has previously
     * been saved, it will retrieve related MovieLinkCategories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Movie.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMovieLinkCategory[] List of ChildMovieLinkCategory objects
     */
    public function getMovieLinkCategoriesJoinCategory(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMovieLinkCategoryQuery::create(null, $criteria);
        $query->joinWith('Category', $joinBehavior);

        return $this->getMovieLinkCategories($query, $con);
    }

    /**
     * Clears out the collMovieLinkProductions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMovieLinkProductions()
     */
    public function clearMovieLinkProductions()
    {
        $this->collMovieLinkProductions = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMovieLinkProductions collection loaded partially.
     */
    public function resetPartialMovieLinkProductions($v = true)
    {
        $this->collMovieLinkProductionsPartial = $v;
    }

    /**
     * Initializes the collMovieLinkProductions collection.
     *
     * By default this just sets the collMovieLinkProductions collection to an empty array (like clearcollMovieLinkProductions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMovieLinkProductions($overrideExisting = true)
    {
        if (null !== $this->collMovieLinkProductions && !$overrideExisting) {
            return;
        }
        $this->collMovieLinkProductions = new ObjectCollection();
        $this->collMovieLinkProductions->setModel('\MovieLinkProduction');
    }

    /**
     * Gets an array of ChildMovieLinkProduction objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMovie is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMovieLinkProduction[] List of ChildMovieLinkProduction objects
     * @throws PropelException
     */
    public function getMovieLinkProductions(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMovieLinkProductionsPartial && !$this->isNew();
        if (null === $this->collMovieLinkProductions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMovieLinkProductions) {
                // return empty collection
                $this->initMovieLinkProductions();
            } else {
                $collMovieLinkProductions = ChildMovieLinkProductionQuery::create(null, $criteria)
                    ->filterByMovie($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMovieLinkProductionsPartial && count($collMovieLinkProductions)) {
                        $this->initMovieLinkProductions(false);

                        foreach ($collMovieLinkProductions as $obj) {
                            if (false == $this->collMovieLinkProductions->contains($obj)) {
                                $this->collMovieLinkProductions->append($obj);
                            }
                        }

                        $this->collMovieLinkProductionsPartial = true;
                    }

                    return $collMovieLinkProductions;
                }

                if ($partial && $this->collMovieLinkProductions) {
                    foreach ($this->collMovieLinkProductions as $obj) {
                        if ($obj->isNew()) {
                            $collMovieLinkProductions[] = $obj;
                        }
                    }
                }

                $this->collMovieLinkProductions = $collMovieLinkProductions;
                $this->collMovieLinkProductionsPartial = false;
            }
        }

        return $this->collMovieLinkProductions;
    }

    /**
     * Sets a collection of ChildMovieLinkProduction objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $movieLinkProductions A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMovie The current object (for fluent API support)
     */
    public function setMovieLinkProductions(Collection $movieLinkProductions, ConnectionInterface $con = null)
    {
        /** @var ChildMovieLinkProduction[] $movieLinkProductionsToDelete */
        $movieLinkProductionsToDelete = $this->getMovieLinkProductions(new Criteria(), $con)->diff($movieLinkProductions);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->movieLinkProductionsScheduledForDeletion = clone $movieLinkProductionsToDelete;

        foreach ($movieLinkProductionsToDelete as $movieLinkProductionRemoved) {
            $movieLinkProductionRemoved->setMovie(null);
        }

        $this->collMovieLinkProductions = null;
        foreach ($movieLinkProductions as $movieLinkProduction) {
            $this->addMovieLinkProduction($movieLinkProduction);
        }

        $this->collMovieLinkProductions = $movieLinkProductions;
        $this->collMovieLinkProductionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MovieLinkProduction objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MovieLinkProduction objects.
     * @throws PropelException
     */
    public function countMovieLinkProductions(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMovieLinkProductionsPartial && !$this->isNew();
        if (null === $this->collMovieLinkProductions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMovieLinkProductions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMovieLinkProductions());
            }

            $query = ChildMovieLinkProductionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMovie($this)
                ->count($con);
        }

        return count($this->collMovieLinkProductions);
    }

    /**
     * Method called to associate a ChildMovieLinkProduction object to this object
     * through the ChildMovieLinkProduction foreign key attribute.
     *
     * @param  ChildMovieLinkProduction $l ChildMovieLinkProduction
     * @return $this|\Movie The current object (for fluent API support)
     */
    public function addMovieLinkProduction(ChildMovieLinkProduction $l)
    {
        if ($this->collMovieLinkProductions === null) {
            $this->initMovieLinkProductions();
            $this->collMovieLinkProductionsPartial = true;
        }

        if (!$this->collMovieLinkProductions->contains($l)) {
            $this->doAddMovieLinkProduction($l);
        }

        return $this;
    }

    /**
     * @param ChildMovieLinkProduction $movieLinkProduction The ChildMovieLinkProduction object to add.
     */
    protected function doAddMovieLinkProduction(ChildMovieLinkProduction $movieLinkProduction)
    {
        $this->collMovieLinkProductions[]= $movieLinkProduction;
        $movieLinkProduction->setMovie($this);
    }

    /**
     * @param  ChildMovieLinkProduction $movieLinkProduction The ChildMovieLinkProduction object to remove.
     * @return $this|ChildMovie The current object (for fluent API support)
     */
    public function removeMovieLinkProduction(ChildMovieLinkProduction $movieLinkProduction)
    {
        if ($this->getMovieLinkProductions()->contains($movieLinkProduction)) {
            $pos = $this->collMovieLinkProductions->search($movieLinkProduction);
            $this->collMovieLinkProductions->remove($pos);
            if (null === $this->movieLinkProductionsScheduledForDeletion) {
                $this->movieLinkProductionsScheduledForDeletion = clone $this->collMovieLinkProductions;
                $this->movieLinkProductionsScheduledForDeletion->clear();
            }
            $this->movieLinkProductionsScheduledForDeletion[]= clone $movieLinkProduction;
            $movieLinkProduction->setMovie(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Movie is new, it will return
     * an empty collection; or if this Movie has previously
     * been saved, it will retrieve related MovieLinkProductions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Movie.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMovieLinkProduction[] List of ChildMovieLinkProduction objects
     */
    public function getMovieLinkProductionsJoinProduction(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMovieLinkProductionQuery::create(null, $criteria);
        $query->joinWith('Production', $joinBehavior);

        return $this->getMovieLinkProductions($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aDirector) {
            $this->aDirector->removeMovie($this);
        }
        $this->title = null;
        $this->year = null;
        $this->director_id = null;
        $this->poster = null;
        $this->backdrop = null;
        $this->rating = null;
        $this->synopsis = null;
        $this->trailer = null;
        $this->runtime = null;
        $this->adult = null;
        $this->id = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCasts) {
                foreach ($this->collCasts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMovieLinkCategories) {
                foreach ($this->collMovieLinkCategories as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMovieLinkProductions) {
                foreach ($this->collMovieLinkProductions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCasts = null;
        $this->collMovieLinkCategories = null;
        $this->collMovieLinkProductions = null;
        $this->aDirector = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MovieTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {

    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
