<?php

namespace Base;

use \MovieLinkCategory as ChildMovieLinkCategory;
use \MovieLinkCategoryQuery as ChildMovieLinkCategoryQuery;
use \Exception;
use \PDO;
use Map\MovieLinkCategoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'movielinkcategory' table.
 *
 *
 *
 * @method     ChildMovieLinkCategoryQuery orderByMovieTitle($order = Criteria::ASC) Order by the movie_title column
 * @method     ChildMovieLinkCategoryQuery orderByCategoryName($order = Criteria::ASC) Order by the category_name column
 *
 * @method     ChildMovieLinkCategoryQuery groupByMovieTitle() Group by the movie_title column
 * @method     ChildMovieLinkCategoryQuery groupByCategoryName() Group by the category_name column
 *
 * @method     ChildMovieLinkCategoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMovieLinkCategoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMovieLinkCategoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMovieLinkCategoryQuery leftJoinMovie($relationAlias = null) Adds a LEFT JOIN clause to the query using the Movie relation
 * @method     ChildMovieLinkCategoryQuery rightJoinMovie($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Movie relation
 * @method     ChildMovieLinkCategoryQuery innerJoinMovie($relationAlias = null) Adds a INNER JOIN clause to the query using the Movie relation
 *
 * @method     ChildMovieLinkCategoryQuery leftJoinCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the Category relation
 * @method     ChildMovieLinkCategoryQuery rightJoinCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Category relation
 * @method     ChildMovieLinkCategoryQuery innerJoinCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the Category relation
 *
 * @method     \MovieQuery|\CategoryQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMovieLinkCategory findOne(ConnectionInterface $con = null) Return the first ChildMovieLinkCategory matching the query
 * @method     ChildMovieLinkCategory findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMovieLinkCategory matching the query, or a new ChildMovieLinkCategory object populated from the query conditions when no match is found
 *
 * @method     ChildMovieLinkCategory findOneByMovieTitle(string $movie_title) Return the first ChildMovieLinkCategory filtered by the movie_title column
 * @method     ChildMovieLinkCategory findOneByCategoryName(string $category_name) Return the first ChildMovieLinkCategory filtered by the category_name column *

 * @method     ChildMovieLinkCategory requirePk($key, ConnectionInterface $con = null) Return the ChildMovieLinkCategory by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMovieLinkCategory requireOne(ConnectionInterface $con = null) Return the first ChildMovieLinkCategory matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMovieLinkCategory requireOneByMovieTitle(string $movie_title) Return the first ChildMovieLinkCategory filtered by the movie_title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMovieLinkCategory requireOneByCategoryName(string $category_name) Return the first ChildMovieLinkCategory filtered by the category_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMovieLinkCategory[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMovieLinkCategory objects based on current ModelCriteria
 * @method     ChildMovieLinkCategory[]|ObjectCollection findByMovieTitle(string $movie_title) Return ChildMovieLinkCategory objects filtered by the movie_title column
 * @method     ChildMovieLinkCategory[]|ObjectCollection findByCategoryName(string $category_name) Return ChildMovieLinkCategory objects filtered by the category_name column
 * @method     ChildMovieLinkCategory[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MovieLinkCategoryQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MovieLinkCategoryQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'VODB', $modelName = '\\MovieLinkCategory', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMovieLinkCategoryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMovieLinkCategoryQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMovieLinkCategoryQuery) {
            return $criteria;
        }
        $query = new ChildMovieLinkCategoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$movie_title, $category_name] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMovieLinkCategory|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = MovieLinkCategoryTableMap::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MovieLinkCategoryTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMovieLinkCategory A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT movie_title, category_name FROM movielinkcategory WHERE movie_title = :p0 AND category_name = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMovieLinkCategory $obj */
            $obj = new ChildMovieLinkCategory();
            $obj->hydrate($row);
            MovieLinkCategoryTableMap::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMovieLinkCategory|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMovieLinkCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(MovieLinkCategoryTableMap::COL_MOVIE_TITLE, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(MovieLinkCategoryTableMap::COL_CATEGORY_NAME, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMovieLinkCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(MovieLinkCategoryTableMap::COL_MOVIE_TITLE, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(MovieLinkCategoryTableMap::COL_CATEGORY_NAME, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the movie_title column
     *
     * Example usage:
     * <code>
     * $query->filterByMovieTitle('fooValue');   // WHERE movie_title = 'fooValue'
     * $query->filterByMovieTitle('%fooValue%'); // WHERE movie_title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $movieTitle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMovieLinkCategoryQuery The current query, for fluid interface
     */
    public function filterByMovieTitle($movieTitle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($movieTitle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $movieTitle)) {
                $movieTitle = str_replace('*', '%', $movieTitle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MovieLinkCategoryTableMap::COL_MOVIE_TITLE, $movieTitle, $comparison);
    }

    /**
     * Filter the query on the category_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryName('fooValue');   // WHERE category_name = 'fooValue'
     * $query->filterByCategoryName('%fooValue%'); // WHERE category_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $categoryName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMovieLinkCategoryQuery The current query, for fluid interface
     */
    public function filterByCategoryName($categoryName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($categoryName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $categoryName)) {
                $categoryName = str_replace('*', '%', $categoryName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MovieLinkCategoryTableMap::COL_CATEGORY_NAME, $categoryName, $comparison);
    }

    /**
     * Filter the query by a related \Movie object
     *
     * @param \Movie|ObjectCollection $movie The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMovieLinkCategoryQuery The current query, for fluid interface
     */
    public function filterByMovie($movie, $comparison = null)
    {
        if ($movie instanceof \Movie) {
            return $this
                ->addUsingAlias(MovieLinkCategoryTableMap::COL_MOVIE_TITLE, $movie->getTitle(), $comparison);
        } elseif ($movie instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MovieLinkCategoryTableMap::COL_MOVIE_TITLE, $movie->toKeyValue('Title', 'Title'), $comparison);
        } else {
            throw new PropelException('filterByMovie() only accepts arguments of type \Movie or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Movie relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMovieLinkCategoryQuery The current query, for fluid interface
     */
    public function joinMovie($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Movie');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Movie');
        }

        return $this;
    }

    /**
     * Use the Movie relation Movie object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MovieQuery A secondary query class using the current class as primary query
     */
    public function useMovieQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMovie($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Movie', '\MovieQuery');
    }

    /**
     * Filter the query by a related \Category object
     *
     * @param \Category|ObjectCollection $category The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMovieLinkCategoryQuery The current query, for fluid interface
     */
    public function filterByCategory($category, $comparison = null)
    {
        if ($category instanceof \Category) {
            return $this
                ->addUsingAlias(MovieLinkCategoryTableMap::COL_CATEGORY_NAME, $category->getName(), $comparison);
        } elseif ($category instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MovieLinkCategoryTableMap::COL_CATEGORY_NAME, $category->toKeyValue('PrimaryKey', 'Name'), $comparison);
        } else {
            throw new PropelException('filterByCategory() only accepts arguments of type \Category or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Category relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMovieLinkCategoryQuery The current query, for fluid interface
     */
    public function joinCategory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Category');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Category');
        }

        return $this;
    }

    /**
     * Use the Category relation Category object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CategoryQuery A secondary query class using the current class as primary query
     */
    public function useCategoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Category', '\CategoryQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMovieLinkCategory $movieLinkCategory Object to remove from the list of results
     *
     * @return $this|ChildMovieLinkCategoryQuery The current query, for fluid interface
     */
    public function prune($movieLinkCategory = null)
    {
        if ($movieLinkCategory) {
            $this->addCond('pruneCond0', $this->getAliasedColName(MovieLinkCategoryTableMap::COL_MOVIE_TITLE), $movieLinkCategory->getMovieTitle(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(MovieLinkCategoryTableMap::COL_CATEGORY_NAME), $movieLinkCategory->getCategoryName(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the movielinkcategory table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovieLinkCategoryTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MovieLinkCategoryTableMap::clearInstancePool();
            MovieLinkCategoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovieLinkCategoryTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MovieLinkCategoryTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MovieLinkCategoryTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MovieLinkCategoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MovieLinkCategoryQuery
