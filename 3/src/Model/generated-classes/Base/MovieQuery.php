<?php

namespace Base;

use \Movie as ChildMovie;
use \MovieQuery as ChildMovieQuery;
use \Exception;
use \PDO;
use Map\MovieTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'movie' table.
 *
 *
 *
 * @method     ChildMovieQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildMovieQuery orderByYear($order = Criteria::ASC) Order by the year column
 * @method     ChildMovieQuery orderByDirectorId($order = Criteria::ASC) Order by the director_id column
 * @method     ChildMovieQuery orderByPoster($order = Criteria::ASC) Order by the poster column
 * @method     ChildMovieQuery orderByBackdrop($order = Criteria::ASC) Order by the backdrop column
 * @method     ChildMovieQuery orderByRating($order = Criteria::ASC) Order by the rating column
 * @method     ChildMovieQuery orderBySynopsis($order = Criteria::ASC) Order by the synopsis column
 * @method     ChildMovieQuery orderByTrailer($order = Criteria::ASC) Order by the trailer column
 * @method     ChildMovieQuery orderByRuntime($order = Criteria::ASC) Order by the runtime column
 * @method     ChildMovieQuery orderByAdult($order = Criteria::ASC) Order by the adult column
 * @method     ChildMovieQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method     ChildMovieQuery groupByTitle() Group by the title column
 * @method     ChildMovieQuery groupByYear() Group by the year column
 * @method     ChildMovieQuery groupByDirectorId() Group by the director_id column
 * @method     ChildMovieQuery groupByPoster() Group by the poster column
 * @method     ChildMovieQuery groupByBackdrop() Group by the backdrop column
 * @method     ChildMovieQuery groupByRating() Group by the rating column
 * @method     ChildMovieQuery groupBySynopsis() Group by the synopsis column
 * @method     ChildMovieQuery groupByTrailer() Group by the trailer column
 * @method     ChildMovieQuery groupByRuntime() Group by the runtime column
 * @method     ChildMovieQuery groupByAdult() Group by the adult column
 * @method     ChildMovieQuery groupById() Group by the id column
 *
 * @method     ChildMovieQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMovieQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMovieQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMovieQuery leftJoinDirector($relationAlias = null) Adds a LEFT JOIN clause to the query using the Director relation
 * @method     ChildMovieQuery rightJoinDirector($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Director relation
 * @method     ChildMovieQuery innerJoinDirector($relationAlias = null) Adds a INNER JOIN clause to the query using the Director relation
 *
 * @method     ChildMovieQuery leftJoinCast($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cast relation
 * @method     ChildMovieQuery rightJoinCast($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cast relation
 * @method     ChildMovieQuery innerJoinCast($relationAlias = null) Adds a INNER JOIN clause to the query using the Cast relation
 *
 * @method     ChildMovieQuery leftJoinMovieLinkCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the MovieLinkCategory relation
 * @method     ChildMovieQuery rightJoinMovieLinkCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MovieLinkCategory relation
 * @method     ChildMovieQuery innerJoinMovieLinkCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the MovieLinkCategory relation
 *
 * @method     ChildMovieQuery leftJoinMovieLinkProduction($relationAlias = null) Adds a LEFT JOIN clause to the query using the MovieLinkProduction relation
 * @method     ChildMovieQuery rightJoinMovieLinkProduction($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MovieLinkProduction relation
 * @method     ChildMovieQuery innerJoinMovieLinkProduction($relationAlias = null) Adds a INNER JOIN clause to the query using the MovieLinkProduction relation
 *
 * @method     \DirectorQuery|\CastQuery|\MovieLinkCategoryQuery|\MovieLinkProductionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMovie findOne(ConnectionInterface $con = null) Return the first ChildMovie matching the query
 * @method     ChildMovie findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMovie matching the query, or a new ChildMovie object populated from the query conditions when no match is found
 *
 * @method     ChildMovie findOneByTitle(string $title) Return the first ChildMovie filtered by the title column
 * @method     ChildMovie findOneByYear(int $year) Return the first ChildMovie filtered by the year column
 * @method     ChildMovie findOneByDirectorId(int $director_id) Return the first ChildMovie filtered by the director_id column
 * @method     ChildMovie findOneByPoster(string $poster) Return the first ChildMovie filtered by the poster column
 * @method     ChildMovie findOneByBackdrop(string $backdrop) Return the first ChildMovie filtered by the backdrop column
 * @method     ChildMovie findOneByRating(double $rating) Return the first ChildMovie filtered by the rating column
 * @method     ChildMovie findOneBySynopsis(string $synopsis) Return the first ChildMovie filtered by the synopsis column
 * @method     ChildMovie findOneByTrailer(string $trailer) Return the first ChildMovie filtered by the trailer column
 * @method     ChildMovie findOneByRuntime(int $runtime) Return the first ChildMovie filtered by the runtime column
 * @method     ChildMovie findOneByAdult(boolean $adult) Return the first ChildMovie filtered by the adult column
 * @method     ChildMovie findOneById(int $id) Return the first ChildMovie filtered by the id column *

 * @method     ChildMovie requirePk($key, ConnectionInterface $con = null) Return the ChildMovie by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMovie requireOne(ConnectionInterface $con = null) Return the first ChildMovie matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMovie requireOneByTitle(string $title) Return the first ChildMovie filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMovie requireOneByYear(int $year) Return the first ChildMovie filtered by the year column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMovie requireOneByDirectorId(int $director_id) Return the first ChildMovie filtered by the director_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMovie requireOneByPoster(string $poster) Return the first ChildMovie filtered by the poster column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMovie requireOneByBackdrop(string $backdrop) Return the first ChildMovie filtered by the backdrop column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMovie requireOneByRating(double $rating) Return the first ChildMovie filtered by the rating column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMovie requireOneBySynopsis(string $synopsis) Return the first ChildMovie filtered by the synopsis column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMovie requireOneByTrailer(string $trailer) Return the first ChildMovie filtered by the trailer column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMovie requireOneByRuntime(int $runtime) Return the first ChildMovie filtered by the runtime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMovie requireOneByAdult(boolean $adult) Return the first ChildMovie filtered by the adult column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMovie requireOneById(int $id) Return the first ChildMovie filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMovie[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMovie objects based on current ModelCriteria
 * @method     ChildMovie[]|ObjectCollection findByTitle(string $title) Return ChildMovie objects filtered by the title column
 * @method     ChildMovie[]|ObjectCollection findByYear(int $year) Return ChildMovie objects filtered by the year column
 * @method     ChildMovie[]|ObjectCollection findByDirectorId(int $director_id) Return ChildMovie objects filtered by the director_id column
 * @method     ChildMovie[]|ObjectCollection findByPoster(string $poster) Return ChildMovie objects filtered by the poster column
 * @method     ChildMovie[]|ObjectCollection findByBackdrop(string $backdrop) Return ChildMovie objects filtered by the backdrop column
 * @method     ChildMovie[]|ObjectCollection findByRating(double $rating) Return ChildMovie objects filtered by the rating column
 * @method     ChildMovie[]|ObjectCollection findBySynopsis(string $synopsis) Return ChildMovie objects filtered by the synopsis column
 * @method     ChildMovie[]|ObjectCollection findByTrailer(string $trailer) Return ChildMovie objects filtered by the trailer column
 * @method     ChildMovie[]|ObjectCollection findByRuntime(int $runtime) Return ChildMovie objects filtered by the runtime column
 * @method     ChildMovie[]|ObjectCollection findByAdult(boolean $adult) Return ChildMovie objects filtered by the adult column
 * @method     ChildMovie[]|ObjectCollection findById(int $id) Return ChildMovie objects filtered by the id column
 * @method     ChildMovie[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MovieQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MovieQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'VODB', $modelName = '\\Movie', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMovieQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMovieQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMovieQuery) {
            return $criteria;
        }
        $query = new ChildMovieQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56), $con);
     * </code>
     *
     * @param array[$title, $year, $director_id] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMovie|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = MovieTableMap::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1], (string) $key[2]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MovieTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMovie A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT title, year, director_id, poster, backdrop, rating, synopsis, trailer, runtime, adult, id FROM movie WHERE title = :p0 AND year = :p1 AND director_id = :p2';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMovie $obj */
            $obj = new ChildMovie();
            $obj->hydrate($row);
            MovieTableMap::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1], (string) $key[2])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMovie|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(MovieTableMap::COL_TITLE, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(MovieTableMap::COL_YEAR, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(MovieTableMap::COL_DIRECTOR_ID, $key[2], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(MovieTableMap::COL_TITLE, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(MovieTableMap::COL_YEAR, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(MovieTableMap::COL_DIRECTOR_ID, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MovieTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the year column
     *
     * Example usage:
     * <code>
     * $query->filterByYear(1234); // WHERE year = 1234
     * $query->filterByYear(array(12, 34)); // WHERE year IN (12, 34)
     * $query->filterByYear(array('min' => 12)); // WHERE year > 12
     * </code>
     *
     * @param     mixed $year The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function filterByYear($year = null, $comparison = null)
    {
        if (is_array($year)) {
            $useMinMax = false;
            if (isset($year['min'])) {
                $this->addUsingAlias(MovieTableMap::COL_YEAR, $year['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($year['max'])) {
                $this->addUsingAlias(MovieTableMap::COL_YEAR, $year['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovieTableMap::COL_YEAR, $year, $comparison);
    }

    /**
     * Filter the query on the director_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDirectorId(1234); // WHERE director_id = 1234
     * $query->filterByDirectorId(array(12, 34)); // WHERE director_id IN (12, 34)
     * $query->filterByDirectorId(array('min' => 12)); // WHERE director_id > 12
     * </code>
     *
     * @see       filterByDirector()
     *
     * @param     mixed $directorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function filterByDirectorId($directorId = null, $comparison = null)
    {
        if (is_array($directorId)) {
            $useMinMax = false;
            if (isset($directorId['min'])) {
                $this->addUsingAlias(MovieTableMap::COL_DIRECTOR_ID, $directorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($directorId['max'])) {
                $this->addUsingAlias(MovieTableMap::COL_DIRECTOR_ID, $directorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovieTableMap::COL_DIRECTOR_ID, $directorId, $comparison);
    }

    /**
     * Filter the query on the poster column
     *
     * Example usage:
     * <code>
     * $query->filterByPoster('fooValue');   // WHERE poster = 'fooValue'
     * $query->filterByPoster('%fooValue%'); // WHERE poster LIKE '%fooValue%'
     * </code>
     *
     * @param     string $poster The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function filterByPoster($poster = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($poster)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $poster)) {
                $poster = str_replace('*', '%', $poster);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MovieTableMap::COL_POSTER, $poster, $comparison);
    }

    /**
     * Filter the query on the backdrop column
     *
     * Example usage:
     * <code>
     * $query->filterByBackdrop('fooValue');   // WHERE backdrop = 'fooValue'
     * $query->filterByBackdrop('%fooValue%'); // WHERE backdrop LIKE '%fooValue%'
     * </code>
     *
     * @param     string $backdrop The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function filterByBackdrop($backdrop = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($backdrop)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $backdrop)) {
                $backdrop = str_replace('*', '%', $backdrop);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MovieTableMap::COL_BACKDROP, $backdrop, $comparison);
    }

    /**
     * Filter the query on the rating column
     *
     * Example usage:
     * <code>
     * $query->filterByRating(1234); // WHERE rating = 1234
     * $query->filterByRating(array(12, 34)); // WHERE rating IN (12, 34)
     * $query->filterByRating(array('min' => 12)); // WHERE rating > 12
     * </code>
     *
     * @param     mixed $rating The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function filterByRating($rating = null, $comparison = null)
    {
        if (is_array($rating)) {
            $useMinMax = false;
            if (isset($rating['min'])) {
                $this->addUsingAlias(MovieTableMap::COL_RATING, $rating['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rating['max'])) {
                $this->addUsingAlias(MovieTableMap::COL_RATING, $rating['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovieTableMap::COL_RATING, $rating, $comparison);
    }

    /**
     * Filter the query on the synopsis column
     *
     * Example usage:
     * <code>
     * $query->filterBySynopsis('fooValue');   // WHERE synopsis = 'fooValue'
     * $query->filterBySynopsis('%fooValue%'); // WHERE synopsis LIKE '%fooValue%'
     * </code>
     *
     * @param     string $synopsis The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function filterBySynopsis($synopsis = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($synopsis)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $synopsis)) {
                $synopsis = str_replace('*', '%', $synopsis);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MovieTableMap::COL_SYNOPSIS, $synopsis, $comparison);
    }

    /**
     * Filter the query on the trailer column
     *
     * Example usage:
     * <code>
     * $query->filterByTrailer('fooValue');   // WHERE trailer = 'fooValue'
     * $query->filterByTrailer('%fooValue%'); // WHERE trailer LIKE '%fooValue%'
     * </code>
     *
     * @param     string $trailer The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function filterByTrailer($trailer = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($trailer)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $trailer)) {
                $trailer = str_replace('*', '%', $trailer);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MovieTableMap::COL_TRAILER, $trailer, $comparison);
    }

    /**
     * Filter the query on the runtime column
     *
     * Example usage:
     * <code>
     * $query->filterByRuntime(1234); // WHERE runtime = 1234
     * $query->filterByRuntime(array(12, 34)); // WHERE runtime IN (12, 34)
     * $query->filterByRuntime(array('min' => 12)); // WHERE runtime > 12
     * </code>
     *
     * @param     mixed $runtime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function filterByRuntime($runtime = null, $comparison = null)
    {
        if (is_array($runtime)) {
            $useMinMax = false;
            if (isset($runtime['min'])) {
                $this->addUsingAlias(MovieTableMap::COL_RUNTIME, $runtime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($runtime['max'])) {
                $this->addUsingAlias(MovieTableMap::COL_RUNTIME, $runtime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovieTableMap::COL_RUNTIME, $runtime, $comparison);
    }

    /**
     * Filter the query on the adult column
     *
     * Example usage:
     * <code>
     * $query->filterByAdult(true); // WHERE adult = true
     * $query->filterByAdult('yes'); // WHERE adult = true
     * </code>
     *
     * @param     boolean|string $adult The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function filterByAdult($adult = null, $comparison = null)
    {
        if (is_string($adult)) {
            $adult = in_array(strtolower($adult), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(MovieTableMap::COL_ADULT, $adult, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MovieTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MovieTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MovieTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query by a related \Director object
     *
     * @param \Director|ObjectCollection $director The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMovieQuery The current query, for fluid interface
     */
    public function filterByDirector($director, $comparison = null)
    {
        if ($director instanceof \Director) {
            return $this
                ->addUsingAlias(MovieTableMap::COL_DIRECTOR_ID, $director->getId(), $comparison);
        } elseif ($director instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MovieTableMap::COL_DIRECTOR_ID, $director->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDirector() only accepts arguments of type \Director or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Director relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function joinDirector($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Director');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Director');
        }

        return $this;
    }

    /**
     * Use the Director relation Director object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \DirectorQuery A secondary query class using the current class as primary query
     */
    public function useDirectorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDirector($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Director', '\DirectorQuery');
    }

    /**
     * Filter the query by a related \Cast object
     *
     * @param \Cast|ObjectCollection $cast the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovieQuery The current query, for fluid interface
     */
    public function filterByCast($cast, $comparison = null)
    {
        if ($cast instanceof \Cast) {
            return $this
                ->addUsingAlias(MovieTableMap::COL_TITLE, $cast->getMovieTitle(), $comparison);
        } elseif ($cast instanceof ObjectCollection) {
            return $this
                ->useCastQuery()
                ->filterByPrimaryKeys($cast->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCast() only accepts arguments of type \Cast or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cast relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function joinCast($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cast');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cast');
        }

        return $this;
    }

    /**
     * Use the Cast relation Cast object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CastQuery A secondary query class using the current class as primary query
     */
    public function useCastQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCast($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cast', '\CastQuery');
    }

    /**
     * Filter the query by a related \MovieLinkCategory object
     *
     * @param \MovieLinkCategory|ObjectCollection $movieLinkCategory the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovieQuery The current query, for fluid interface
     */
    public function filterByMovieLinkCategory($movieLinkCategory, $comparison = null)
    {
        if ($movieLinkCategory instanceof \MovieLinkCategory) {
            return $this
                ->addUsingAlias(MovieTableMap::COL_TITLE, $movieLinkCategory->getMovieTitle(), $comparison);
        } elseif ($movieLinkCategory instanceof ObjectCollection) {
            return $this
                ->useMovieLinkCategoryQuery()
                ->filterByPrimaryKeys($movieLinkCategory->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMovieLinkCategory() only accepts arguments of type \MovieLinkCategory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MovieLinkCategory relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function joinMovieLinkCategory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MovieLinkCategory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MovieLinkCategory');
        }

        return $this;
    }

    /**
     * Use the MovieLinkCategory relation MovieLinkCategory object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MovieLinkCategoryQuery A secondary query class using the current class as primary query
     */
    public function useMovieLinkCategoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMovieLinkCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MovieLinkCategory', '\MovieLinkCategoryQuery');
    }

    /**
     * Filter the query by a related \MovieLinkProduction object
     *
     * @param \MovieLinkProduction|ObjectCollection $movieLinkProduction the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMovieQuery The current query, for fluid interface
     */
    public function filterByMovieLinkProduction($movieLinkProduction, $comparison = null)
    {
        if ($movieLinkProduction instanceof \MovieLinkProduction) {
            return $this
                ->addUsingAlias(MovieTableMap::COL_TITLE, $movieLinkProduction->getMovieTitle(), $comparison);
        } elseif ($movieLinkProduction instanceof ObjectCollection) {
            return $this
                ->useMovieLinkProductionQuery()
                ->filterByPrimaryKeys($movieLinkProduction->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMovieLinkProduction() only accepts arguments of type \MovieLinkProduction or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MovieLinkProduction relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function joinMovieLinkProduction($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MovieLinkProduction');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MovieLinkProduction');
        }

        return $this;
    }

    /**
     * Use the MovieLinkProduction relation MovieLinkProduction object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MovieLinkProductionQuery A secondary query class using the current class as primary query
     */
    public function useMovieLinkProductionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMovieLinkProduction($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MovieLinkProduction', '\MovieLinkProductionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMovie $movie Object to remove from the list of results
     *
     * @return $this|ChildMovieQuery The current query, for fluid interface
     */
    public function prune($movie = null)
    {
        if ($movie) {
            $this->addCond('pruneCond0', $this->getAliasedColName(MovieTableMap::COL_TITLE), $movie->getTitle(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(MovieTableMap::COL_YEAR), $movie->getYear(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(MovieTableMap::COL_DIRECTOR_ID), $movie->getDirectorId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the movie table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovieTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MovieTableMap::clearInstancePool();
            MovieTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovieTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MovieTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MovieTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MovieTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MovieQuery
