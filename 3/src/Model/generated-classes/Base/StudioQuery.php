<?php

namespace Base;

use \Studio as ChildStudio;
use \StudioQuery as ChildStudioQuery;
use \Exception;
use \PDO;
use Map\StudioTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'studio' table.
 *
 *
 *
 * @method     ChildStudioQuery orderByProductionId($order = Criteria::ASC) Order by the production_id column
 * @method     ChildStudioQuery orderByName($order = Criteria::ASC) Order by the name column
 *
 * @method     ChildStudioQuery groupByProductionId() Group by the production_id column
 * @method     ChildStudioQuery groupByName() Group by the name column
 *
 * @method     ChildStudioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildStudioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildStudioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildStudioQuery leftJoinProduction($relationAlias = null) Adds a LEFT JOIN clause to the query using the Production relation
 * @method     ChildStudioQuery rightJoinProduction($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Production relation
 * @method     ChildStudioQuery innerJoinProduction($relationAlias = null) Adds a INNER JOIN clause to the query using the Production relation
 *
 * @method     \ProductionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildStudio findOne(ConnectionInterface $con = null) Return the first ChildStudio matching the query
 * @method     ChildStudio findOneOrCreate(ConnectionInterface $con = null) Return the first ChildStudio matching the query, or a new ChildStudio object populated from the query conditions when no match is found
 *
 * @method     ChildStudio findOneByProductionId(int $production_id) Return the first ChildStudio filtered by the production_id column
 * @method     ChildStudio findOneByName(string $name) Return the first ChildStudio filtered by the name column *

 * @method     ChildStudio requirePk($key, ConnectionInterface $con = null) Return the ChildStudio by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStudio requireOne(ConnectionInterface $con = null) Return the first ChildStudio matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStudio requireOneByProductionId(int $production_id) Return the first ChildStudio filtered by the production_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStudio requireOneByName(string $name) Return the first ChildStudio filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStudio[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildStudio objects based on current ModelCriteria
 * @method     ChildStudio[]|ObjectCollection findByProductionId(int $production_id) Return ChildStudio objects filtered by the production_id column
 * @method     ChildStudio[]|ObjectCollection findByName(string $name) Return ChildStudio objects filtered by the name column
 * @method     ChildStudio[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class StudioQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\StudioQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'VODB', $modelName = '\\Studio', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildStudioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildStudioQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildStudioQuery) {
            return $criteria;
        }
        $query = new ChildStudioQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildStudio|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = StudioTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StudioTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStudio A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT production_id, name FROM studio WHERE production_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildStudio $obj */
            $obj = new ChildStudio();
            $obj->hydrate($row);
            StudioTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildStudio|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildStudioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(StudioTableMap::COL_PRODUCTION_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildStudioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(StudioTableMap::COL_PRODUCTION_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the production_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProductionId(1234); // WHERE production_id = 1234
     * $query->filterByProductionId(array(12, 34)); // WHERE production_id IN (12, 34)
     * $query->filterByProductionId(array('min' => 12)); // WHERE production_id > 12
     * </code>
     *
     * @see       filterByProduction()
     *
     * @param     mixed $productionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStudioQuery The current query, for fluid interface
     */
    public function filterByProductionId($productionId = null, $comparison = null)
    {
        if (is_array($productionId)) {
            $useMinMax = false;
            if (isset($productionId['min'])) {
                $this->addUsingAlias(StudioTableMap::COL_PRODUCTION_ID, $productionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productionId['max'])) {
                $this->addUsingAlias(StudioTableMap::COL_PRODUCTION_ID, $productionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StudioTableMap::COL_PRODUCTION_ID, $productionId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStudioQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StudioTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query by a related \Production object
     *
     * @param \Production|ObjectCollection $production The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStudioQuery The current query, for fluid interface
     */
    public function filterByProduction($production, $comparison = null)
    {
        if ($production instanceof \Production) {
            return $this
                ->addUsingAlias(StudioTableMap::COL_PRODUCTION_ID, $production->getId(), $comparison);
        } elseif ($production instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(StudioTableMap::COL_PRODUCTION_ID, $production->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProduction() only accepts arguments of type \Production or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Production relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStudioQuery The current query, for fluid interface
     */
    public function joinProduction($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Production');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Production');
        }

        return $this;
    }

    /**
     * Use the Production relation Production object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProductionQuery A secondary query class using the current class as primary query
     */
    public function useProductionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProduction($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Production', '\ProductionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildStudio $studio Object to remove from the list of results
     *
     * @return $this|ChildStudioQuery The current query, for fluid interface
     */
    public function prune($studio = null)
    {
        if ($studio) {
            $this->addUsingAlias(StudioTableMap::COL_PRODUCTION_ID, $studio->getProductionId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the studio table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StudioTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            StudioTableMap::clearInstancePool();
            StudioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StudioTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(StudioTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            StudioTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            StudioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // StudioQuery
