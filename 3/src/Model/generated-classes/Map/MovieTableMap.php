<?php

namespace Map;

use \Movie;
use \MovieQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'movie' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class MovieTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.MovieTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'VODB';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'movie';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Movie';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Movie';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'movie.title';

    /**
     * the column name for the year field
     */
    const COL_YEAR = 'movie.year';

    /**
     * the column name for the director_id field
     */
    const COL_DIRECTOR_ID = 'movie.director_id';

    /**
     * the column name for the poster field
     */
    const COL_POSTER = 'movie.poster';

    /**
     * the column name for the backdrop field
     */
    const COL_BACKDROP = 'movie.backdrop';

    /**
     * the column name for the rating field
     */
    const COL_RATING = 'movie.rating';

    /**
     * the column name for the synopsis field
     */
    const COL_SYNOPSIS = 'movie.synopsis';

    /**
     * the column name for the trailer field
     */
    const COL_TRAILER = 'movie.trailer';

    /**
     * the column name for the runtime field
     */
    const COL_RUNTIME = 'movie.runtime';

    /**
     * the column name for the adult field
     */
    const COL_ADULT = 'movie.adult';

    /**
     * the column name for the id field
     */
    const COL_ID = 'movie.id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Title', 'Year', 'DirectorId', 'Poster', 'Backdrop', 'Rating', 'Synopsis', 'Trailer', 'Runtime', 'Adult', 'Id', ),
        self::TYPE_CAMELNAME     => array('title', 'year', 'directorId', 'poster', 'backdrop', 'rating', 'synopsis', 'trailer', 'runtime', 'adult', 'id', ),
        self::TYPE_COLNAME       => array(MovieTableMap::COL_TITLE, MovieTableMap::COL_YEAR, MovieTableMap::COL_DIRECTOR_ID, MovieTableMap::COL_POSTER, MovieTableMap::COL_BACKDROP, MovieTableMap::COL_RATING, MovieTableMap::COL_SYNOPSIS, MovieTableMap::COL_TRAILER, MovieTableMap::COL_RUNTIME, MovieTableMap::COL_ADULT, MovieTableMap::COL_ID, ),
        self::TYPE_FIELDNAME     => array('title', 'year', 'director_id', 'poster', 'backdrop', 'rating', 'synopsis', 'trailer', 'runtime', 'adult', 'id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Title' => 0, 'Year' => 1, 'DirectorId' => 2, 'Poster' => 3, 'Backdrop' => 4, 'Rating' => 5, 'Synopsis' => 6, 'Trailer' => 7, 'Runtime' => 8, 'Adult' => 9, 'Id' => 10, ),
        self::TYPE_CAMELNAME     => array('title' => 0, 'year' => 1, 'directorId' => 2, 'poster' => 3, 'backdrop' => 4, 'rating' => 5, 'synopsis' => 6, 'trailer' => 7, 'runtime' => 8, 'adult' => 9, 'id' => 10, ),
        self::TYPE_COLNAME       => array(MovieTableMap::COL_TITLE => 0, MovieTableMap::COL_YEAR => 1, MovieTableMap::COL_DIRECTOR_ID => 2, MovieTableMap::COL_POSTER => 3, MovieTableMap::COL_BACKDROP => 4, MovieTableMap::COL_RATING => 5, MovieTableMap::COL_SYNOPSIS => 6, MovieTableMap::COL_TRAILER => 7, MovieTableMap::COL_RUNTIME => 8, MovieTableMap::COL_ADULT => 9, MovieTableMap::COL_ID => 10, ),
        self::TYPE_FIELDNAME     => array('title' => 0, 'year' => 1, 'director_id' => 2, 'poster' => 3, 'backdrop' => 4, 'rating' => 5, 'synopsis' => 6, 'trailer' => 7, 'runtime' => 8, 'adult' => 9, 'id' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('movie');
        $this->setPhpName('Movie');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Movie');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('title', 'Title', 'VARCHAR', true, 255, null);
        $this->addPrimaryKey('year', 'Year', 'INTEGER', true, null, null);
        $this->addForeignPrimaryKey('director_id', 'DirectorId', 'INTEGER' , 'director', 'id', true, null, null);
        $this->addColumn('poster', 'Poster', 'VARCHAR', false, 255, null);
        $this->addColumn('backdrop', 'Backdrop', 'VARCHAR', false, 255, null);
        $this->addColumn('rating', 'Rating', 'FLOAT', false, null, null);
        $this->addColumn('synopsis', 'Synopsis', 'LONGVARCHAR', false, null, null);
        $this->addColumn('trailer', 'Trailer', 'VARCHAR', false, 255, null);
        $this->addColumn('runtime', 'Runtime', 'INTEGER', false, null, null);
        $this->addColumn('adult', 'Adult', 'BOOLEAN', false, 1, null);
        $this->addColumn('id', 'Id', 'INTEGER', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Director', '\\Director', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':director_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Cast', '\\Cast', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':movie_title',
    1 => ':title',
  ),
), null, null, 'Casts', false);
        $this->addRelation('MovieLinkCategory', '\\MovieLinkCategory', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':movie_title',
    1 => ':title',
  ),
), null, null, 'MovieLinkCategories', false);
        $this->addRelation('MovieLinkProduction', '\\MovieLinkProduction', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':movie_title',
    1 => ':title',
  ),
), null, null, 'MovieLinkProductions', false);
    } // buildRelations()

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database. In some cases you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by find*()
     * and findPk*() calls.
     *
     * @param \Movie $obj A \Movie object.
     * @param string $key             (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (null === $key) {
                $key = serialize(array((string) $obj->getTitle(), (string) $obj->getYear(), (string) $obj->getDirectorId()));
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param mixed $value A \Movie object or a primary key value.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && null !== $value) {
            if (is_object($value) && $value instanceof \Movie) {
                $key = serialize(array((string) $value->getTitle(), (string) $value->getYear(), (string) $value->getDirectorId()));

            } elseif (is_array($value) && count($value) === 3) {
                // assume we've been passed a primary key";
                $key = serialize(array((string) $value[0], (string) $value[1], (string) $value[2]));
            } elseif ($value instanceof Criteria) {
                self::$instances = [];

                return;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or \Movie object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Year', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 2 + $offset : static::translateFieldName('DirectorId', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return serialize(array((string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)], (string) $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Year', TableMap::TYPE_PHPNAME, $indexType)], (string) $row[TableMap::TYPE_NUM == $indexType ? 2 + $offset : static::translateFieldName('DirectorId', TableMap::TYPE_PHPNAME, $indexType)]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
            $pks = [];

        $pks[] = (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 1 + $offset
                : self::translateFieldName('Year', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 2 + $offset
                : self::translateFieldName('DirectorId', TableMap::TYPE_PHPNAME, $indexType)
        ];

        return $pks;
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? MovieTableMap::CLASS_DEFAULT : MovieTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Movie object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = MovieTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = MovieTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + MovieTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = MovieTableMap::OM_CLASS;
            /** @var Movie $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            MovieTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = MovieTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = MovieTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Movie $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                MovieTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(MovieTableMap::COL_TITLE);
            $criteria->addSelectColumn(MovieTableMap::COL_YEAR);
            $criteria->addSelectColumn(MovieTableMap::COL_DIRECTOR_ID);
            $criteria->addSelectColumn(MovieTableMap::COL_POSTER);
            $criteria->addSelectColumn(MovieTableMap::COL_BACKDROP);
            $criteria->addSelectColumn(MovieTableMap::COL_RATING);
            $criteria->addSelectColumn(MovieTableMap::COL_SYNOPSIS);
            $criteria->addSelectColumn(MovieTableMap::COL_TRAILER);
            $criteria->addSelectColumn(MovieTableMap::COL_RUNTIME);
            $criteria->addSelectColumn(MovieTableMap::COL_ADULT);
            $criteria->addSelectColumn(MovieTableMap::COL_ID);
        } else {
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.year');
            $criteria->addSelectColumn($alias . '.director_id');
            $criteria->addSelectColumn($alias . '.poster');
            $criteria->addSelectColumn($alias . '.backdrop');
            $criteria->addSelectColumn($alias . '.rating');
            $criteria->addSelectColumn($alias . '.synopsis');
            $criteria->addSelectColumn($alias . '.trailer');
            $criteria->addSelectColumn($alias . '.runtime');
            $criteria->addSelectColumn($alias . '.adult');
            $criteria->addSelectColumn($alias . '.id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(MovieTableMap::DATABASE_NAME)->getTable(MovieTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(MovieTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(MovieTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new MovieTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Movie or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Movie object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovieTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Movie) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(MovieTableMap::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(MovieTableMap::COL_TITLE, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(MovieTableMap::COL_YEAR, $value[1]));
                $criterion->addAnd($criteria->getNewCriterion(MovieTableMap::COL_DIRECTOR_ID, $value[2]));
                $criteria->addOr($criterion);
            }
        }

        $query = MovieQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            MovieTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                MovieTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the movie table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return MovieQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Movie or Criteria object.
     *
     * @param mixed               $criteria Criteria or Movie object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MovieTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Movie object
        }


        // Set the correct dbName
        $query = MovieQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // MovieTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
MovieTableMap::buildTableMap();
