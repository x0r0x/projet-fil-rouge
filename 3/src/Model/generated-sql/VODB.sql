
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- movie
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `movie`;

CREATE TABLE `movie`
(
    `title` VARCHAR(255) NOT NULL,
    `year` INTEGER NOT NULL,
    `director_id` INTEGER NOT NULL,
    `poster` VARCHAR(255),
    `backdrop` VARCHAR(255),
    `rating` FLOAT,
    `synopsis` TEXT,
    `trailer` VARCHAR(255),
    `runtime` INTEGER,
    `adult` TINYINT(1),
    `id` INTEGER,
    PRIMARY KEY (`title`,`year`,`director_id`),
    INDEX `movie_fi_2cc047` (`director_id`),
    CONSTRAINT `movie_fk_2cc047`
        FOREIGN KEY (`director_id`)
        REFERENCES `director` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- director
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `director`;

CREATE TABLE `director`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `firstname` VARCHAR(255) NOT NULL,
    `lastname` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- category
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category`
(
    `name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`name`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- actor
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `actor`;

CREATE TABLE `actor`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `firstname` VARCHAR(255) NOT NULL,
    `lastname` VARCHAR(255) NOT NULL,
    `thumbnail` BLOB,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cast
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cast`;

CREATE TABLE `cast`
(
    `movie_title` VARCHAR(255) NOT NULL,
    `actor_id` INTEGER NOT NULL,
    PRIMARY KEY (`movie_title`,`actor_id`),
    INDEX `cast_fi_a575e2` (`actor_id`),
    CONSTRAINT `cast_fk_b53c5d`
        FOREIGN KEY (`movie_title`)
        REFERENCES `movie` (`title`),
    CONSTRAINT `cast_fk_a575e2`
        FOREIGN KEY (`actor_id`)
        REFERENCES `actor` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- production
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `production`;

CREATE TABLE `production`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- producer
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `producer`;

CREATE TABLE `producer`
(
    `production_id` INTEGER NOT NULL,
    `firstname` VARCHAR(255) NOT NULL,
    `lastname` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`production_id`),
    CONSTRAINT `producer_fk_79cb83`
        FOREIGN KEY (`production_id`)
        REFERENCES `production` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- studio
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `studio`;

CREATE TABLE `studio`
(
    `production_id` INTEGER NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`production_id`),
    CONSTRAINT `studio_fk_79cb83`
        FOREIGN KEY (`production_id`)
        REFERENCES `production` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- movielinkcategory
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `movielinkcategory`;

CREATE TABLE `movielinkcategory`
(
    `movie_title` VARCHAR(255) NOT NULL,
    `category_name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`movie_title`,`category_name`),
    INDEX `movielinkcategory_fi_a0d3c0` (`category_name`),
    CONSTRAINT `movielinkcategory_fk_b53c5d`
        FOREIGN KEY (`movie_title`)
        REFERENCES `movie` (`title`),
    CONSTRAINT `movielinkcategory_fk_a0d3c0`
        FOREIGN KEY (`category_name`)
        REFERENCES `category` (`name`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- movielinkproduction
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `movielinkproduction`;

CREATE TABLE `movielinkproduction`
(
    `movie_title` VARCHAR(255) NOT NULL,
    `production_id` INTEGER NOT NULL,
    PRIMARY KEY (`movie_title`,`production_id`),
    INDEX `movielinkproduction_fi_79cb83` (`production_id`),
    CONSTRAINT `movielinkproduction_fk_b53c5d`
        FOREIGN KEY (`movie_title`)
        REFERENCES `movie` (`title`),
    CONSTRAINT `movielinkproduction_fk_79cb83`
        FOREIGN KEY (`production_id`)
        REFERENCES `production` (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
