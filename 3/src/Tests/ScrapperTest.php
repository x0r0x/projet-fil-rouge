<?php
require __DIR__ . '/../../vendor/autoload.php';

$scrapper = new Scrapper\Scrapper();

print_r($scrapper->getMovieById(1272));

echo "Affiches disponibles pour le film d'ID 550 :\n";
var_dump ($scrapper->getMoviePostersById(550, "large"));

echo "Résultats pour la recherche 'toto' :\n";
 var_dump ($scrapper->searchMovieByName("toto"));

echo "Api configuration :\n";
var_dump ($scrapper->getApiConfiguration());
var_dump ($scrapper->getMoviePostersById(550, "large"));

/* Test sans cache */
$scrapper = new Scrapper\Scrapper();
var_dump($scrapper->searchMovieByName('terminator'));