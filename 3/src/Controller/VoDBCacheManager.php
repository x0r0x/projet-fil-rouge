<?php

namespace VoDBCacheManager;

use phpFastCache\CacheManager;

class VoDBCacheManager
{
    private $cache;
    
    public function __construct() {
        $this->cache = CacheManager::Files();
    }

    /**
     * @return mixed|string
     */
    public function getCacheStats() {
        $cacheStats = $this->cache->stats();
        if (count($cacheStats['data'])) return $cacheStats;
        return "Cache is empty";
    }

    /**
     * @return string
     */
    public function dropCache() {
        $this->cache->clean();
        return "Cache dropped";
    }
}
