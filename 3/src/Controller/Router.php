<?php

namespace Router;
use Exception;

class Router
{       
    private $uri;
    private $routes = array();
    
    public function __construct($uri) {
        $this->uri = $uri;
    }

    /** Ajoute une un couple {pattern, callable}
     * @param $key
     * @param callable $callable
     */
    public function addRoute($key, callable $callable) {
        $this->routes[$key] = $callable;
    }

    /** Appel le premier callable matchant le pattern
     */
    public function dispatch() {
        foreach ($this->routes as $key => $callback) {
            if (preg_match($key, $this->uri, $uri)) {
                try {
                    $callback($uri);
                    break;
                } catch (Exception $e) {
                    echo($e->getMessage()."\n");
                }
            }
        }
    }
}