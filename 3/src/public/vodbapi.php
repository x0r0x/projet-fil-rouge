<?php

require __DIR__ . '/../../vendor/autoload.php';

if (!isset($_SERVER['REQUEST_URI']))
    return;

$router = new Router\Router($_SERVER['REQUEST_URI']);

$router->addRoute('/get\/movie\/(\d+)/', function($uri) {
    if(array_key_exists('src', $_GET) && $_GET['src'] === "internal") {
        $library = Library\LibraryFactory::getInstance();
        $movie = $library->findMovieById($uri[1]);
        if ($movie !== null){
            echo(json_encode($movie));
        }
    } else {
        $scrapper = Scrapper\CachedScrapperFactory::getInstance();
        echo(json_encode($scrapper->getMovieById($uri[1])));
    }
});

$router->addRoute('/search\/movie\/([a-zA-Z0-9_%\+]+)/', function($uri) {

    if(array_key_exists('src', $_GET)) {
        $src = $_GET['src'];
    } else {
        // Default to external
        $src = "internal";
    }

    $dataLocator = new VoDBService\DataLocatorService($src);
    header('Content-type: application/json');
    echo json_encode($dataLocator->search($uri[1]));
});

$router->addRoute('/lists\/movie\/popular/', function()
{
    if(!array_key_exists('limit', $_GET)) {
        $limit = 0;
    } else {
        $limit = $_GET['limit'];
    }
    if(array_key_exists('src', $_GET)) {
        $src = $_GET['src'];
    } else {
        // Default to internal
        $src = "internal";
    }

    $dataLocator = new VoDBService\DataLocatorService($src);
    return $dataLocator->getPopular($limit);
});

$router->addRoute('/get\/posters\/(\d+)/', function($uri) {
    if (!isset($_GET)) return;

    $scrapper = Scrapper\CachedScrapperFactory::getInstance();
    if(!array_key_exists('size', $_GET)) {
        $size = "large";
    } else {
        $size = $_GET['size'];
    }
    echo(json_encode($scrapper->getMoviePostersById($uri[1], $size)));
});

$router->addRoute('/cache\/report$/', function() {
    $report = new VoDBCacheManager\VoDBCacheManager();
    print_r($report->getCacheStats());
});

$router->addRoute('/cache\/drop$/', function() {
    $report = new VoDBCacheManager\VoDBCacheManager();
    echo($report->dropCache());
});


$router->addRoute('/update\/popularity(?:\/(\d+))?/', function($uri) {
    $limit = 0;
    if (count($uri) >= 2) {
        $limit = $uri[1];
    }

    $storage = new VoDBService\StorageService();
    $storage->storePopularity($limit);
});

$router->addRoute('/store\/movie\/(\d+)/', function($uri) {
    $storage = new VoDBService\StorageService();
    try {
	   $storage->storeMovieById($uri[1]);
    } catch (Exception $e) {
        http_response_code(503);
    }
});

$router->addRoute('/\bauth\b$/', function() {
   if (
       !isset($_POST) ||
       !array_key_exists("id", $_POST) ||
       !array_key_exists("hash", $_POST))
   {
       http_response_code(404);
   }
   try {

   } catch (Exception $e) {
   }
});

$router->dispatch();
