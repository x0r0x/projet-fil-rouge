<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script src="js/jquery-2.2.0.min.js"></script>
    <script src="js/getflix.js"></script>
    <link rel="stylesheet" type="text/css" href="css/getflix.css">
</head>
<body>
<div class="header">
    <a href="index.html" id="logo"></a>
</div>
<div class="moviePresentationTitle">
    <?php
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $json = json_decode(file_get_contents('http://localhost/get/movie/' . $id . '?src=internal'));
            $title = $json->Title;
            if (!empty($title)){
                echo ("Lecture du film \"" . $title . "\"");
                ?>
		<div style="margin-top:25px;">
                    <video width="720" height="480" controls>
                    Your browser does not support the video tag.
                    </video>
                </div>
                <?php
            } else {
                echo ("Impossible de récupérer le titre du film");
            }
        }
    ?>
</div>
</body>
</html>



