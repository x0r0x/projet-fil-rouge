function rowCreator(movieSelector,rootSelector)
{
    // On garde une référence de la derniere collection de film qu'on nous a donné
    // pour pouvoir réordonné les ligne sans avoir besoin de refounir la liste des film en mode "brute"
    var rawFilmCollection = [], movieDivWidth = $(movieSelector).width();

    /*
     * Méthod qui s'occupe de la création des lignes
     *
     * @param array   $filmCollection La collection de film
     * @param integer $itemByRow      Le nombre d'item par ligne souhaité
     */
    this.createRow = function(itemByRow, filmCollection)
    {
        // Si on ne nous fournie pas de "filmCollection" cela veut dire qu'on demande un mise a jour simple de la grille
        if (filmCollection !== undefined) {
            rawFilmCollection = filmCollection;
        }

        var rowCollection = [], row = [];

        // On parcours la collection de film
        $(rawFilmCollection).each(function(index, film) {
            // Si le maximum est atteint, on crée un nouvelle ligne
            if (itemByRow == row.length) {
                rowCollection.push(row);
                // On réinitialise la ligne
                row = [];
            }

            row.push(film);
        });

        // Si il y a des film dans la derniere ligne, crée ($itemByRow pas atteint) on ajoute la derniere ligne
        if (row.length !== 0) {
            rowCollection.push(row);
        }

        return rowCollection;
    };

    /*
     * Méthode permettant de récupérer le nombre d'item par ligne possible en fonction de la taille de l'écran
     */
    this.getItemByRow = function()
    {
        return Math.floor($(rootSelector).width() / movieDivWidth) ;
    };
}

function movieSearchDao()
{
    var ajaxQueryCollection = [];

    this.storeMovie = function(id) {
        $.ajax({
            type: 'GET',
            url: "../store/movie/" + id,
            dataType: 'html',
            cache: false,
            timeout: 5000,
            success: function(data)
            {
                alert(data);
            }
        });
    }

    this.updatePopularity = function(limit)
    {
        $.ajax({
            type: 'GET',
            url: "../update/popularity/" + limit,
            dataType: 'json',
            cache: false,
            timeout: 5000
        });
    }

    this.search = function(title, src) {
        deferred=this.ajaxQuery('../search/movie/' + title, src);
        return deferred.promise();
    }

    this.getPopularsMovies = function(src)
    {
        deferred=this.ajaxQuery("../lists/movie/popular",src);
        return deferred.promise();
    }

    this.ajaxQuery = function(url, src)
    {
        ajaxQueryCollection.forEach(function(query)
        {
            query.abort();
        });

        var param = "";
        if (src === "external")
        {
            param = "?src=external";
            picsPath = "http://image.tmdb.org/t/p/w300";
        } else if (src === "internal")
        {
            param = "?src=internal";
            picsPath = "../pics/backdrops/";
        }

        var deferred = $.Deferred();

        ajaxQueryCollection.push(
            $.ajax({
                type: 'GET',
                url: url + param,
                dataType: 'json',
                cache: false,
                timeout: 5000,
                success: function (movieCollection) {
                    var movieCollectionLoaded = [];
                    $(movieCollection).each(function(key, value){

                        var movieBackdrop = new Image();
                        value.backdrop_path = picsPath + value.backdrop_path;
                        movieBackdrop.onload = function () {
                            movieCollectionLoaded.push(value);
                            deferred.notify(movieCollectionLoaded);
                        };
                        movieBackdrop.src = value.backdrop_path;
                    });
                }
            }));


        return deferred;
    }
}