#!/bin/bash

if [ ! -e src/mysql-dsn.conf ] || [ ! -e propel.json ]; then
	exit 1
fi

db_ip=$(grep mysql:host src/mysql-dsn.conf | grep -Eo '([0-9]{1,3}\.){3}[0-9]{1,3}')
sed -i "s/\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\}/$db_ip/" propel.json

./vendor/bin/propel model:build --schema-dir src/Model/schema/ --output-dir src/Model/generated-classes
./vendor/bin/propel sql:build --schema-dir src/Model/schema/ --output-dir src/Model/generated-sql --overwrite
./vendor/bin/propel sql:insert --sql-dir=src/Model/generated-sql/ --config-dir=.

composer dumpautoload

echo "Done!"