<?php

require __DIR__ . '/../vendor/autoload.php';

if (chdir("src/public/") != true) {
    die("cannot chdir");
}

// Remplissage initial de la base de films
echo("Création du jeux de films initial : \n");
$storageService = new VoDBService\StorageService();
$movieList = array ("terminator", "matrix", "the", "little", "le seigneur des anneaux", "tintin");
$storageService = new VoDBService\StorageService();
$storageService->fillDatabase($movieList);


// Récupération des films populaires et remplissage de la base
echo("Création des films populaires : \n");
$scrapper = \Scrapper\CachedScrapperFactory::getInstance();
$popularMovies = array();

foreach ($scrapper->getPopularMovies(100) as $movie) {
    sleep (1);
    $storageService->storeMovieById($movie['id']);
    array_push($popularMovies, $movie['id']);
}
