package getflixpayment.delegates;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.Serializable;

@Named
@ApplicationScoped
public class Routeur implements Serializable {

    private static final long serialVersionUID = 99984565L;

    private final String host = "http://localhost";

    private final String movie_api = getHost().concat("/get/movie/");

    private final String source = "internal";


    public void redirect(String url) throws IOException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        externalContext.redirect(url);
    }

    public void redispatch(String url) throws ServletException, IOException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        externalContext.dispatch(url);
    }


    public String getHost() {
        return host;
    }

    public String getMovie_api() {
        return movie_api;
    }

    public String getSource() {
        return source;
    }
}
