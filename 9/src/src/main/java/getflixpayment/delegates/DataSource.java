package getflixpayment.delegates;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@Named
@ApplicationScoped
public class DataSource implements Serializable {

    private static final long serialVersionUID = 90875466L;

    public Connection provideConnection() throws ClassNotFoundException, SQLException {
        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:getflix.db");
        return c;
    }

}
