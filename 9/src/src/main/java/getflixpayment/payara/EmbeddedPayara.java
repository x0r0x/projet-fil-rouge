package getflixpayment.payara;

import fish.payara.micro.BootstrapException;
import fish.payara.micro.PayaraMicro;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class EmbeddedPayara {

    public static void main(String[] args)  throws BootstrapException {
        //String warLocation = "/home/gaarv/code/getflixpayment/target/getflixpayment.war";
        String warLoation = "C:\\Users\\ishy7496\\Documents\\code\\getflixpayment\\target\\getflixpayment.war";
        PayaraMicro.getInstance()
                .setHttpPort(8080)
                .addDeployment(warLoation)
                .bootStrap();

        // connection sqllite
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:getflix.db");
            stmt = c.createStatement();

            String sql = "DROP TABLE IF EXISTS user";
            stmt.executeUpdate(sql);

            sql = "DROP TABLE IF EXISTS movie_collection";
            stmt.executeUpdate(sql);

            sql = "CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT NOT NULL, balance DECIMAL NOT NULL)";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user VALUES (null, 'getflixUser', 0.0);";
            stmt.executeUpdate(sql);

            sql = "CREATE TABLE movie_collection (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT NOT NULL, title VARCHAR NOT NULL, buydate VARCHAR NOT NULL, movie_id VARCHAR NOT NULL)";
            stmt.executeUpdate(sql);

            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");

    }

}
