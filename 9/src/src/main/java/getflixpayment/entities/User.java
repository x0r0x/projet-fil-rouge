package getflixpayment.entities;



public class User {

    private Integer id = 1;
    private String username = "getflixUser";
    private Double balance = 0.0;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Double getBalance() {
        String credit = String.format("%.2f", balance).replace(",", ".");
        balance = new Double(credit);
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
