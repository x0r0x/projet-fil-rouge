package getflixpayment.entities;


import getflixpayment.delegates.Routeur;

import javax.inject.Inject;
import java.io.Serializable;

public class Movie implements Serializable {

    private static final long serialVersionUID = 666687454L;

    @Inject
    private Routeur routeur;

    private String images_path = "http://localhost";

    private Long id;
    private String title;
    private String poster;
    private String backdrop;
    private String synopsis;
    private Double rating;
    private String year;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = getImages_path().concat("/pics/backdrops/").concat(backdrop);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = getImages_path().concat("/pics/posters/").concat(poster);
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    private String getImages_path() {
        return images_path;
    }

    private void setImages_path(String images_path) {
        this.images_path = images_path;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", poster='" + poster + '\'' +
                ", backdrop='" + backdrop + '\'' +
                ", synopsis='" + synopsis + '\'' +
                ", rating=" + rating +
                ", year='" + year + '\'' +
                '}';
    }
}
