package getflixpayment.entities;

import java.io.Serializable;

public class Transaction implements Serializable {

    private static final long serialVersionUID = 5512599666L;


    private String id;
    private String title;
    private String date;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
