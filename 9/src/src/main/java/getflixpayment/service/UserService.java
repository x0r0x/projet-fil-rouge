package getflixpayment.service;

import getflixpayment.entities.User;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.sql.*;


@Named
@SessionScoped
public class UserService implements Serializable {

    private static final long serialVersionUID = 603918994L;

    public User getUser() throws ClassNotFoundException, SQLException, IOException {
        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:getflix.db");
        PreparedStatement ps = c.prepareStatement("SELECT id, username, balance FROM user WHERE username == 'getflixUser'");
        ResultSet rs = ps.executeQuery();
        User user = new User();
        while (rs.next()) {

            user.setId(rs.getInt("id"));
            user.setUsername(rs.getString("username"));
            user.setBalance(rs.getDouble("balance"));
        }

        ps.close();
        c.close();
        return user;
    }


}
