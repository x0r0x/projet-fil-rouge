package getflixpayment.service;

import getflixpayment.delegates.DataSource;
import getflixpayment.delegates.Routeur;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.validation.constraints.Digits;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Named
@SessionScoped
public class PaymentService implements Serializable {

    private static final long serialVersionUID = 451257854L;

    @Inject
    private Routeur routeur;

    @Inject
    private DataSource dataSource;

    @Inject
    private MovieService movieService;

    @Digits(integer = 10, fraction = 2, message = "Montant invalide")
    private Double value = 50.00;

    private Double price = 12.95;

    public void creditAccount() throws SQLException, ClassNotFoundException, ServletException, IOException {
        saveAccountMovement(this.value);
        routeur.redispatch("index.xhtml");
    }

    public Boolean debitAccount() throws SQLException, ClassNotFoundException, ServletException, IOException {
        if (checkForSufficientCredit(price)) {
            saveAccountMovement(-this.price);
            return true;
        } else {
            return false;
        }
    }

    public void saveAccountMovement(Double amount) throws SQLException, ClassNotFoundException {
        Connection c = dataSource.provideConnection();
        String sql = "UPDATE user SET balance = ? WHERE username == 'getflixUser'";
        Double newAmount = amount + getBalance();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setDouble(1, newAmount);
        ps.executeUpdate();
        ps.close();
        c.close();
    }


    public Boolean checkForSufficientCredit(Double value) throws SQLException, ClassNotFoundException {
        Double balance = getBalance();
        Boolean sufficientCredit = false;
        if (balance >= Double.valueOf(value.toString())) {
            sufficientCredit = true;
        } else {
            sufficientCredit = false;
        }
        return sufficientCredit;
    }


    public Double getBalance() throws ClassNotFoundException, SQLException {
        Connection c = dataSource.provideConnection();
        PreparedStatement ps = c.prepareStatement("SELECT balance FROM user WHERE username == 'getflixUser'");
        ResultSet rs = ps.executeQuery();
        Double amount = 0.0;
        while (rs.next()) {
            amount = rs.getDouble("balance");
        }
        ps.close();
        c.close();
        return amount;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}