package getflixpayment.service;


import getflixpayment.client.RestClient;
import getflixpayment.delegates.DataSource;
import getflixpayment.delegates.Routeur;
import getflixpayment.entities.Movie;
import getflixpayment.entities.Transaction;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Named
@SessionScoped
public class MovieService implements Serializable {

    private static final long serialVersionUID = 54111224L;

    @Inject
    private RestClient restClient;

    @Inject
    private DataSource dataSource;

    @Inject
    private PaymentService paymentService;

    @Inject
    private Routeur routeur;

    private String id;

    private Movie movie;

    private List<Transaction> collection = new ArrayList<>();

    private Boolean movieOwned = false;

    public void getMovieInfos() {
        Movie movie = (Movie) restClient.toObject(
                routeur.getMovie_api()
                        .concat(this.id)
                        .concat("?src=")
                        .concat(routeur.getSource()),
                Movie.class);
        movie.setPoster(movie.getPoster());
        movie.setBackdrop(movie.getBackdrop());
        setMovie(movie);
    }
    public void saveMovieToCollection() throws SQLException, ClassNotFoundException {
        getMovieInfos();
        Connection c = dataSource.provideConnection();
        String query = String.format("INSERT INTO movie_collection VALUES (null, 'getflixUser', '%s', '%s', '%s');",
                movie.getTitle(),
                new Date().toString(),
                movie.getId().toString()
        );
        String sql = query;
        PreparedStatement ps = c.prepareStatement(sql);
        ps.executeUpdate();
        ps.close();
        c.close();
    }

    public void getMovieCollection() throws ClassNotFoundException, SQLException, IOException {
        List<Transaction> collection = new ArrayList<>();
        Connection c = dataSource.provideConnection();
        String sql = "SELECT title, buydate, movie_id FROM movie_collection WHERE username == 'getflixUser' ORDER BY buydate DESC";
        PreparedStatement ps = c.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Transaction transaction = new Transaction();
            transaction.setTitle(rs.getString("title"));
            transaction.setDate(rs.getString("buydate"));
            transaction.setId(rs.getString("movie_id"));
            collection.add(transaction);
        }
        ps.close();
        c.close();
        setCollection(collection);
    }

    public void buyMovie() throws ClassNotFoundException, SQLException, ServletException, IOException {
        clearMessages();
        getMovieInfos();
        Boolean purchased = paymentService.debitAccount();
        if (purchased) {
            saveMovieToCollection();
            String message = "Merci de votre achat !";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(message));
        } else {
            String message = "Fonds insuffisants, merci d'approvisionner votre compte.";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(message));
        }
    }

    private void clearMessages() {
        Iterator iter = FacesContext.getCurrentInstance().getMessages();
        while (iter.hasNext()) {
            iter.remove();
        }
    }

    public void isMovieOwned() throws SQLException, IOException, ClassNotFoundException {
        List<String> transactions = new ArrayList<>();
        for (Transaction transaction : collection) {
            transactions.add(transaction.getTitle());
        }
        movieOwned = transactions.contains(movie.getTitle());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

     public Boolean getMovieOwned() {
        return movieOwned;
    }

    public void setMovieOwned(Boolean movieOwned) {
        this.movieOwned = movieOwned;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public List<Transaction> getCollection() {
        return collection;
    }

    public void setCollection(List<Transaction> collection) {
        this.collection = collection;
    }
}
