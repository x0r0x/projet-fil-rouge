package getflixpayment.client;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.lang.reflect.Type;


@ApplicationScoped
public class RestClient {

    CloseableHttpClient client;

    //HttpHost target = new HttpHost("proxyweb.utc.fr", 3128, "http");

    @PostConstruct
    private void init() {
        this.client = HttpClientBuilder.create()
                .build();
    }

    public String executeQuery(String s) throws IOException {
        HttpResponse response = client.execute(new HttpGet(s));
        String responseAsString = EntityUtils.toString(response.getEntity());
        return responseAsString;
    }

    public Object toObject(String url, Object object) {
        String response = "";
        try {
            response = executeQuery(url);
        } catch (IOException | IllegalStateException e) {
            e.printStackTrace();
        }
        Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
        Object mappedObject = gson.fromJson(response, (Type) object);
        return mappedObject;
    }


}
