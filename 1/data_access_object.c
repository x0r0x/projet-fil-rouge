#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include "data_access_object.h"

data_transfer_object_t * __load_data_from_file ( const char * filename )
{
	/* TODO :
	- Improve the way to handle file path 
	*/
	struct stat st;
	char * file_path = malloc ( sizeof ( char ) * strlen ( DATADIR ) );
	strcpy ( file_path, DATADIR );
	file_path = realloc ( file_path, strlen ( file_path ) + strlen ( filename ) );
	strcat ( file_path, filename );
	
	data_transfer_object_t * dto = (data_transfer_object_t *) ( malloc ( sizeof ( data_transfer_object_t ) ) );
	
	if ( stat ( file_path, &st)  == -1 )
	{
		perror ( "__load_data_from_file/stat_file"  );
		return ( NULL );
	}

	if ( ( file_input = fopen ( file_path, "rb") ) == NULL )
	{
		perror ( "__load_data_from_file/fopen" );
		return ( NULL );
	}	
		
	dto->bytes_read = st.st_size;
	dto->data = (char *) malloc ( sizeof ( char ) * st.st_size ); 	
	fread ( dto->data, st.st_size, 1, file_input );
	fclose ( file_input );
	
	return dto;
}

data_transfer_object_t * __load_data_from_processus ( const char * path, const char * bin, const char * args )
{
	data_transfer_object_t * dto = (data_transfer_object_t *) ( malloc ( sizeof ( data_transfer_object_t ) ) );

	FILE *fp;
	int status;
	char cmd_with_args [ strlen ( path ) + strlen ( bin ) + strlen ( args ) + 1 ];
	strcpy ( cmd_with_args, path );
	strcat ( cmd_with_args, bin );
	strcat ( cmd_with_args, " " );
	strcat ( cmd_with_args, args );

	fp = popen ( cmd_with_args, "r" );
	if ( fp == NULL )
	{
		perror ( "processus_output/popen error" );
		return NULL;
	}

	char buffer [ 512 ];
	dto->bytes_read = 0;
	dto->data = NULL;
	dto->bytes_read = 0;
	char * tmp_ptr = NULL;
	int	strlength;

	while ( fgets ( buffer, sizeof ( buffer ), fp) != NULL)
	{
		strlength = strlen ( buffer );
		tmp_ptr = realloc ( dto->data, dto->bytes_read + strlength );
		dto->data = tmp_ptr;
		strcpy ( dto->data + dto->bytes_read, buffer);     // append buffer to str
		dto->bytes_read += strlength; 
	}

	status = pclose ( fp );

	if ( status == -1 )
		perror ( "processus_output/pclose" );

	return dto;
}

int __send_buffered_data ( int * socket_fd, data_transfer_object_t * dto,  int buffer_size )
{
	if ( buffer_size > MTU )
		return 1;
	
	if ( dto->bytes_read > buffer_size )
	{
		int chunk_num 	= dto->bytes_read / buffer_size;
		int last_chunk 	= dto->bytes_read % buffer_size;
		char * data_ptr 	= dto->data;
		int i = 0;
		
		for ( i = 0; i < chunk_num; i++ )
		{
			if ( send ( *socket_fd, data_ptr, buffer_size, 0 ) != buffer_size )
			{
				return ( 1 );
			}
			data_ptr += buffer_size;
		}	
		
		if ( send ( *socket_fd, data_ptr, last_chunk, 0 ) != last_chunk )
		{
			return ( 1 );
		}		
	} else
	{
		if ( send ( *socket_fd, dto->data, dto->bytes_read, 0 ) != dto->bytes_read )
		{
			return ( 1 );
		}
	}

	return ( 0 );
}

void __free_dto ( data_transfer_object_t * dto )
{
	if ( sizeof ( * dto ) )
	{
		free ( dto->data );
		free ( dto );
	}
}

/*
int __write_buffered_data_to_file ( const char * filename, data_transfer_object_t dto,  int buffer_size )
{
	if ( buffer_size > MTU )
		return 1

	char * buffer;	

	if ( ( file_input = fopen ( filename, "wb") ) == NULL )
	{
		perror ( "__write_file/fopen" );
		exit ( EXIT_FAILURE );
	}

	fseek ( file_input, SEEK_SET, 0 );
	
	if ( dto.bytes_read > buffer_size )
	{
		while ( fread ( buffer, MTU * sizeof ( char ) , 1, file_input ) )
		{
			fwrite ( buffer, MTU * sizeof ( char ), 1, fp_o );
		}
		
		int modulo = st.st_size % MTU;
		char * sub_buffer = ( char * ) malloc ( sizeof ( char ) * modulo );
		fread ( sub_buffer, modulo * sizeof ( char ) , 1, file_input );
		fwrite ( sub_buffer, modulo * sizeof ( char ), 1, fp_o );
		free ( sub_buffer ) ;
	
	} else
	{
		char * sub_buffer = ( char * ) malloc ( sizeof ( char ) * st.st_size );
		fread ( sub_buffer, st.st_size * sizeof ( char ) , 1, file_input );
		fwrite ( sub_buffer, st.st_size * sizeof ( char ), 1, fp_o );
		free ( sub_buffer ) ;
	}

	fclose ( file_input );
	fclose ( fp_o );

	exit ( 0 );
}


int main ()
{
	data_transfer_object_t * dto = __load_data_from_file ( "file1.txt" );
	printf ( "%d bytes\n", dto->bytes_read );
	exit ( 0 );
}
*/
