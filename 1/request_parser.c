#include <regex.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "request_parser.h"

const char * string_regex = "^(GETFILE|FILELIST):(([-._[:alnum:]])+)";

char ** __tokenize_string ( const char * string_regex,  const char * string )
{
	regex_t regex;
	
	if ( regcomp ( &regex, string_regex, REG_EXTENDED ) != 0 )
	{
		perror ( "__tokenize_string/regcomp");
		exit ( EXIT_FAILURE );
	}

	size_t nmatch = regex.re_nsub;
	regmatch_t pmatch [ nmatch + 1 ];	
	char ** tokenized_string = (char ** ) malloc ( sizeof ( char * ) * nmatch );

	if ( regexec ( &regex, string, nmatch + 1, pmatch, 0) != REG_NOMATCH )
	{
		int size, i;
		char * token = NULL;
		for ( i = 1; i <= nmatch; i++)
		{
			if ( (int) pmatch [ i ].rm_so < 0 ) break;
			size = (int) pmatch [ i ].rm_eo - (int) pmatch [ i ].rm_so;
			token = malloc ( sizeof ( char ) * size );
			strncpy ( token, string + pmatch [ i ].rm_so, size );
			token [ size ] = '\0';
			//printf ( "From %d to %d (%s)\n", (int) pmatch [ i ].rm_so, (int) pmatch [ i ].rm_eo, token );
			tokenized_string [ i - 1 ] = token;
		}
	}
		
	regfree ( &regex );
	return ( tokenized_string );
}

void __free_tokenized_string ( char ** char_v_v ) 
{
	if ( char_v_v [ 0 ] )
	{
		int i;
	    int s = sizeof ( char_v_v ) / sizeof ( char_v_v [ 0 ] );

		for ( i = 0; i < s; i++ )
		{
			free ( char_v_v [ i ] );
		}
		free ( char_v_v );
	}
}

/*
int main ( int argc, char ** argv )
{
	const char * regex 				= "(GET).*([0-9]{4})";
	const char * received_data 	= "GET......1234";
	char ** tokenized_data 		= __tokenize_string ( regex, received_data );
	

	// TODO Unallocate properly
	if ( tokenized_data )
	{
		// strcomp ( ...
		// strcomp ( ...
		int i, s = sizeof ( tokenized_data ) / sizeof ( tokenized_data [ 0 ] );
		printf ( "%d\n", s	 );
		for ( i = 0; i < s; i++ )
		{
			free ( tokenized_data [ i ] );
		}
		free ( tokenized_data );
	}
  
	return 0;
}
*/
