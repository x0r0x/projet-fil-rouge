#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <regex.h>
#include <dirent.h> 
#include "data_access_object.h"
#include "request_parser.h"

#define CONNECTION_QUEUE_SIZE 1024
#define PORT	11111
#define BUFFER_SIZE 1024
#define ERROR "ERROR"

/* TODO :
- Daemonize properly
- Refactor signal handling
*/

int exit_main_loop = 0;
int serv_sockfd, client_sockfd;

void sigint_handler (int sig)
{
	signal ( sig, SIG_IGN );
	printf ( "SIGINT intercepted, exiting...\n" );
	exit_main_loop = 1;
	while ( waitpid ( -1, NULL, WNOHANG ) > 0);
	close ( serv_sockfd );
	exit ( EXIT_SUCCESS );
}

int main ( int argc, char ** argv )
{
	int addrin_size = sizeof ( struct sockaddr_in );
	struct sockaddr_in srv_sockaddr, client_sockaddr;
	char buffer [BUFFER_SIZE];
	int reuseaddr = 1;
	
	signal ( SIGINT, sigint_handler );
	
	memset ( buffer, '\0', sizeof ( buffer ) );

	if ( ( serv_sockfd = socket ( AF_INET , SOCK_STREAM , 0 ) ) == -1 )
	{
		perror ("server/main/socket");
		exit ( EXIT_FAILURE );
	}

	srv_sockaddr.sin_family 			= AF_INET;
    srv_sockaddr.sin_addr.s_addr 	= INADDR_ANY;
    srv_sockaddr.sin_port 				= htons ( PORT );
	
	if ( setsockopt ( serv_sockfd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof ( int ) ) == -1 )
	{
        perror ( "server/main/setsockopt" );
        exit ( EXIT_FAILURE );
    }

	if ( bind ( serv_sockfd, ( struct sockaddr * ) &srv_sockaddr , sizeof ( srv_sockaddr ) ) < 0)
    {
        perror ("server/main/bind");
		exit ( EXIT_FAILURE );
    }

	if ( listen ( serv_sockfd, CONNECTION_QUEUE_SIZE ) == -1 )
    {
        perror ("server/main/listen");
		exit ( EXIT_FAILURE );
    }
    printf ("+ Waiting for inboud connections \n");

	while ( ! exit_main_loop )
	{	
		if ( ( client_sockfd = accept ( serv_sockfd, (struct sockaddr *) &client_sockaddr, (socklen_t *) &addrin_size ) ) == -1 )
		{
			perror ( "server/main/accept : error while creating client socket" );
		}
		
		pid_t pid = fork ();

		if ( pid )
		{
			close ( client_sockfd );
			continue; 
		} else if ( pid == 0 )
		{			
			signal ( SIGINT, SIG_IGN );
			close ( serv_sockfd );
			int client_id = getpid ();
			char * client_ip = inet_ntoa ( client_sockaddr.sin_addr );
			int client_port = ntohs ( client_sockaddr.sin_port );
			char ** tokenized_data;
			printf ( "|Connection from %s:%d (id %d)\n", client_ip, client_port, client_id);
			
			recv ( client_sockfd, buffer, sizeof ( buffer ), 0 );
			
			tokenized_data = __tokenize_string ( string_regex, buffer );
			const char * client_primitive = tokenized_data [ 0 ];

			if ( strcmp ( client_primitive, "GETFILE" ) == 0 )
			{
				if ( sizeof ( tokenized_data [ 1 ] ) )
				{
					char * filename = tokenized_data [ 1 ];
					printf ( "|_Get file %s for client ID %d\n", filename, client_id );
					data_transfer_object_t * dto = NULL;

					char * file_path = malloc ( sizeof ( char ) * strlen ( DATADIR ) );
					strcpy ( file_path, DATADIR );
					file_path = realloc ( file_path, strlen ( file_path ) + strlen ( filename ) );
					strcat ( file_path, filename );
					
					if ( ( dto = __load_data_from_processus ( SCRIPTS_DIR, SCRIPT_NAME, file_path ) ) ) // Switch to __load_data_from_file to read directly from a file
					{
						if ( __send_buffered_data ( &client_sockfd, dto, MTU ) == -1 )
						{
							perror ( "server_child/Error while sending data" );
						}
            			__free_dto ( dto );
					} else
					{
						perror ( "server_child/Error while loading data from file" );
					}			
				}
				else 
				{
					send ( client_sockfd, "Error processing data", 6, 0 );
				}
				
			} else if ( strcmp ( client_primitive, "FILELIST" ) == 0 )
			{
				printf ( "|_Get file list for client ID %d\n", client_id );
				DIR * dir = NULL;
				struct dirent * dirent = NULL;
				if ( ( dir = opendir ( DATADIR) ) == NULL )
				{
					perror ( "server_child/list_dir/opendir" );
					exit ( EXIT_FAILURE );
				}
	
				const char * string_regex = ".html$";
				const char * cr = "\n";	
				char * buffer = NULL;
				regex_t regex;
	
				if ( regcomp ( &regex, string_regex, REG_EXTENDED ) != 0 )
				{
					perror ( "server_child/list_dir/regcomp" );
					exit ( EXIT_FAILURE );
				}   
	
				while ( ( dirent = readdir ( dir ) ) != NULL)
				{
					if ( regexec ( &regex, dirent -> d_name, 0, NULL, 0) == 0 && dirent -> d_type == DT_REG )
					{
						int d_name_len = ( (int) strlen ( dirent -> d_name ) ) + 1;
						if ( ( buffer = (char *) malloc ( sizeof ( char ) * d_name_len ) ) == NULL )
						{
							perror ( "server_child/list_file/malloc" );
							regfree ( &regex );
							exit ( EXIT_FAILURE );
						}
						strcpy ( buffer, dirent -> d_name );
						strcat ( buffer, cr );
						if ( send ( client_sockfd, buffer, d_name_len, 0 ) != d_name_len )
						{
							perror ( "server_child/list_dir/send" );
							exit ( EXIT_FAILURE );
						}
						free ( buffer );
					}
				}
	
				regfree ( &regex );
				closedir ( dir );
			}

     		__free_tokenized_string ( tokenized_data );

			close ( client_sockfd );
			exit ( EXIT_SUCCESS );
		} else
		{
			perror ( "server/main/fork" );
			close ( serv_sockfd );
			exit ( EXIT_FAILURE);
		}
	}

	close ( serv_sockfd );
	exit ( EXIT_SUCCESS );
}
