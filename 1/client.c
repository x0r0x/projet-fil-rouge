#include <sys/socket.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <wait.h>
#include "data_access_object.h"
#include "user_iface.h"

int main ( int argc, char ** argv )
{
	if ( argc < 3 ) 
	{
		perror ( "client/main/incorrect arguments : IP, port" );
		exit ( EXIT_FAILURE );
	}	

	char * srv_addr = argv [ 1 ];
	unsigned short int srv_port = ( unsigned short int ) atoi ( argv [ 2 ] );
	
	int client_sockfd;
	char buffer [ MTU ];
	struct sockaddr_in srv_sockaddr;
	FILE * file_output;

	srv_sockaddr.sin_family 		= AF_INET;
    srv_sockaddr.sin_addr.s_addr 	= inet_addr ( srv_addr );
	srv_sockaddr.sin_port 			= htons ( srv_port );

	int exit_main_loop = 0;
    int user_choice = 0;
	char user_input [ 256 ];	

	while ( ! exit_main_loop )
    {            
		__print_menu ();	

		fgets ( user_input, sizeof ( user_input ), stdin );
        __clean_string ( user_input );
	
        user_choice = atoi ( user_input );

		char * srv_order = NULL;
		char file_name [ 256 ];
		int rbytes = 0;
		int total_rbytes = 0;
		char * primitive;

		switch ( user_choice )
		{
			case 1:
				; // Bug GCC 5.2.0 ...
				primitive = "FILELIST:0";
				srv_order = malloc ( sizeof ( char ) * ( (int) strlen ( primitive ) ) );
				strcpy ( srv_order, primitive );
				strcpy ( file_name, "FILELST" );
				break;
			case 2 : // GETFILE
				;
				primitive = "GETFILE:";
				int i;

				for ( i = 2; i < strlen ( user_input ); i++ )
				{
					file_name [ i - 2 ] = user_input [ i ];
				}
				file_name [ i - 1 ] = '\0';
				srv_order = malloc ( sizeof ( char ) * ( strlen ( primitive ) + strlen ( file_name ) ) );
				strcpy ( srv_order, primitive );
				strcat ( srv_order, file_name );	
				break;		
			case 3 :
				exit_main_loop = 1;
				continue;
			default:
				__print_input_error;
				continue;
		}

		if ( ( client_sockfd = socket ( AF_INET , SOCK_STREAM , 0 ) ) == -1 )
		{
			perror ("client/main/socket");
			exit ( EXIT_FAILURE );
		}

		if ( connect ( client_sockfd,  ( struct sockaddr * ) &srv_sockaddr, sizeof ( srv_sockaddr ) ) )
		{
			perror ("client/main/connect");
			close ( client_sockfd );
			exit ( EXIT_FAILURE );
		} 

		send ( client_sockfd, srv_order, strlen ( srv_order ), 0 );

		free ( srv_order );

		// TODO supprimer fichier si existant ?
		if ( ( file_output = fopen ( file_name, "wb") ) == NULL )
		{
			perror ( "client/main/fopen" );
			close ( client_sockfd );
			exit ( EXIT_FAILURE );
		}

		while ( ( rbytes = recv ( client_sockfd, buffer, MTU, 0 ) ) )
		{
			fwrite ( buffer, rbytes, 1, file_output );
			total_rbytes += rbytes;
		}	

		fclose ( file_output );
		
		close ( client_sockfd );	

		if ( total_rbytes == 0 )
		{
			printf ( "0 Byte received from server %s!\n", srv_addr );
		} else
		{
		 	pid_t pid = fork ();
			if ( pid )
			{
				int status;
				wait ( &status );
				char command [ 1024 ];
				strcpy ( command, "rm -f " );
				strcat ( command , file_name );
				system ( command );
				continue;
			} else if ( pid == 0 )
			{		
				close ( client_sockfd );	
				int status;
				char * args [] = { "less", file_name, NULL };

				if ( ( status = execvp ( args [ 0 ], args ) ) == -1 )
				{
					perror ( "client/main/execlp" );	
				}
			} else
			{
				perror ( "client/main/fork" );
			}
		}
	}

	exit ( EXIT_SUCCESS );
}
