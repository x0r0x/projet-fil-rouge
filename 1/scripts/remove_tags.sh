#!/bin/bash

[ ${#1} -gt 0 ] && [ -e $1 ] && sed -e "s/<script>.*<\/script>//" -e "s/<[^>]\+>//g" $1

exit $?
