#ifndef __REQUEST_PARSER_H__
#define __REQUEST_PARSER_H__

const char * string_regex;

char ** __tokenize_string ( const char * string_regex,  const char * string );

void __free_tokenized_string ( char ** char_v_v ); 

#endif
