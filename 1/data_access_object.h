#ifndef __DATA_ACCESS_OBJECT_H__
#define __DATA_ACCESS_OBJECT_H__

#include <stdio.h>
#define MTU 1500
#define DATADIR "./data_files/"
#define SCRIPTS_DIR "./scripts/"
#define SCRIPT_NAME "remove_tags.sh"

FILE * file_input;

typedef struct data_transfer_object_t
{
	int bytes_read;
	char * data;
} data_transfer_object_t;

/**
	Charge le contenu binaire de filename (lecture binaire) dans un dto
*/
data_transfer_object_t * __load_data_from_file ( const char * filename );

/**
	Charge le contenu binaire de filename (lecture binaire) dans un dto, depuis un processus tiers
*/
data_transfer_object_t * __load_data_from_processus ( const char * path, const char * bin, const char * args );

/**
	Envoi sur socket_fd les données de dto par bloc de buffer_size bytes
*/
int __send_buffered_data ( int * socket_fd, data_transfer_object_t * dto,  int buffer_size );

/**
	Désalloue le champ data et la structure pointée par * dto
*/
void __free_dto ( data_transfer_object_t * dto );

#endif
