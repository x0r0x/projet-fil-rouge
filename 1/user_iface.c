#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "user_iface.h"

void __clean_string ( char * string )
{
    char * p_ch = strchr ( string, '\n');
    if ( p_ch != NULL )
    {	// Replace first occurence of \n by \0
        * p_ch = '\0';
    } else
    {	// Consume remaining chars in stdin
        int car;    
        while ( ( car = getchar () ) != '\n' && car != EOF );
    }
}

void __print_menu ()
{
	printf( "1. Liste des fichiers                    : taper 1\n"
	        "2. Telecharger un fichier de ref. xxxxxx : taper 2 xxxxxx\n"
	        "3. Quitter                               : taper 3\n\n"
	        "> Commande : ");
}
